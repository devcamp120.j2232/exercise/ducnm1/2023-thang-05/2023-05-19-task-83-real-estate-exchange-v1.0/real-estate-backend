package com.devcamp.realestateexchangev1.ExceptionHandlers;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.devcamp.realestateexchangev1.Models.CustomReponseBody;

@ControllerAdvice
public class ValidationExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
            HttpHeaders headers, HttpStatus status, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();

        body.put("type", "MethodArgumentNotValidException");
        body.put("timestamp", new Date());
        body.put("status", status.value());
        // Get all errors
        List<String> errors = ex.getBindingResult().getFieldErrors().stream()
                .map(x -> x.getDefaultMessage()).collect(Collectors.toList());
        body.put("error", errors);

        String path = request.getDescription(false).replace("uri=", "");
        body.put("path", path);
            
        CustomReponseBody customReponseBody = new CustomReponseBody("Lỗi tham số", body);

        return new ResponseEntity<>(customReponseBody, headers, status);
    }
}
