package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//generate entity base on this 
//CREATE TABLE `address_map` (
//     `id` int(11) NOT NULL,
//     `address` varchar(5000) NOT NULL,
//     `_lat` double NOT NULL COMMENT 'Vĩ độ',
//     `_lng` double NOT NULL COMMENT 'Kinh độ'
//   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='địa chỉ và tọa độ căn nhà';
@Data
@Entity
@Table(name = "address_map")
@NoArgsConstructor
@AllArgsConstructor
public class AddressMap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "address cannot be empty")
    private String address;

    @NotNull(message = "lat cannot be empty")
    @Column(name = "_lat", nullable = false)
    private double lat;

    @NotNull(message = "lng cannot be empty")
    @Column(name = "_lng", nullable = false)
    private double lng;

    // when delete address, delete all real estate relate to this address
    @PreRemove
    private void removeAddressMap() {
        System.out.println("remove address map");
    }

}
