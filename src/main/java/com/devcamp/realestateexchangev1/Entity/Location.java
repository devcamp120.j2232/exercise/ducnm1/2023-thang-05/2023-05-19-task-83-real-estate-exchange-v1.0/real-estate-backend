package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `locations` (
  `ID` int(11) NOT NULL,
  `Latitude` float NOT NULL DEFAULT 0,
  `Longitude` float NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

// generate java entity base on this

@Data
@Entity
@Table(name = "locations")
@NoArgsConstructor
@AllArgsConstructor
public class Location {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "Latitude")
    private float latitude;

    @Column(name = "Longitude")
    private float longitude;

}