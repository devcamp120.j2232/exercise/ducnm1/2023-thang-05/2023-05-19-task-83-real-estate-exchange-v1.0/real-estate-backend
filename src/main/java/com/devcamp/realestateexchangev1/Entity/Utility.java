package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;

/*
 * 
CREATE TABLE `utilities` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL COMMENT 'tên tiện ích dự án',
  `description` text DEFAULT NULL COMMENT 'mô tả chung tiện ích',
  `photo` varchar(5000) DEFAULT NULL COMMENT 'ảnh tiện ích của án'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='danh sách tiện ích của dự án. Các dự án dùng chung tiện này.';
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

// generate java entity base on this

@Data
@Entity
@Table(name = "utilities")
@NoArgsConstructor
@AllArgsConstructor
public class Utility {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "Tên tiện ích không được để trống")
    private String name;

    @Column(nullable = true)
    private String description;

    @Column(nullable = true)
    private String photo;
}
