package com.devcamp.realestateexchangev1.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `realestate` (
  `id` int(11) NOT NULL,
  `title` varchar(2000) DEFAULT NULL COMMENT 'Tiêu đề tin',
  `type` int(2) DEFAULT NULL COMMENT 'danh mục tin đăng: \r\n0.Đất, \r\n1.Nhà ở,\r\n2.Căn hộ/Chung cư,\r\n3.Văn phòng, Mặt bằng 4.kinh doanh,\r\n5.Phòng trọ,',
  `request` int(2) DEFAULT NULL COMMENT 'Nhu cầu\r\n0.Cần bán\r\n2.Cần mua\r\n3.Cho thuê\r\n4.Cần thuê',
  `province_id` int(2) DEFAULT NULL COMMENT 'Tỉnh thành phố',
  `district_id` int(2) DEFAULT NULL COMMENT 'Quận huyện',
  `wards_id` int(2) DEFAULT NULL COMMENT 'Phường/Xã',
  `street_id` int(2) DEFAULT NULL COMMENT 'Thông tin tên đường',
  `project_id` int(2) DEFAULT NULL COMMENT 'Nhà thuộc dự án nào.',
  `address` varchar(2000) NOT NULL COMMENT 'Địa chỉ chi tiết nhà.',
  `customer_id` int(11) DEFAULT NULL COMMENT 'id người đăng tin bđs bằng độ dài số CMT',
  `price` bigint(60) DEFAULT NULL COMMENT 'Giá hiện tại đăng tin',
  `price_min` bigint(60) DEFAULT NULL COMMENT 'Giá bán tối thiểu',
  `price_time` tinyint(2) DEFAULT NULL COMMENT 'Thời gian bán: bán nhanh, bán chậm2',
  `date_create` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Ngày đăng tin',
  `acreage` decimal(12,2) DEFAULT NULL COMMENT 'diện tích bđs, diện tích thông thủy',
  `direction` int(2) DEFAULT NULL COMMENT 'Hướng nhà, căn hộ\r\nTây Bắc: 0,\r\nTây: 1,\r\nBắc: 2,\r\nĐông: 3,\r\nNam: 4,\r\nTây Nam: 5,\r\nĐông Bắc: 6,\r\nĐông Nam: 7,\r\nTây Nam, Đông Nam: 8,\r\nTây Nam, Tây Bắc:9,\r\nKhông rõ: 10,\r\nĐông Bắc, Đông Nam: 11,\r\nĐông Bắc, Tây Bắc: 12.',
  `total_floors` int(2) DEFAULT NULL COMMENT 'Tổng số tầng nhà có',
  `number_floors` int(2) DEFAULT NULL COMMENT 'Tầng số bao nhiêu, giành cho bán căn hộ cc',
  `bath` int(2) DEFAULT NULL COMMENT 'Số nhà vệ sinh có',
  `apart_code` varchar(10) DEFAULT NULL COMMENT 'apartment code, mã căn hộ',
  `wall_area` decimal(12,2) DEFAULT NULL COMMENT 'Diện tích tim tường',
  `bedroom` tinyint(4) DEFAULT NULL COMMENT 'Số phòng ngủ',
  `balcony` tinyint(4) DEFAULT NULL COMMENT 'Ban công, lô gia',
  `landscape_view` varchar(255) DEFAULT NULL COMMENT 'view cảnh quan,\r\n0: chưa cập nhật,\r\n1: hồ bơi',
  `apart_loca` tinyint(4) DEFAULT NULL COMMENT 'apartment location, vi trí căn hộ: căn góc, căn thường',
  `apart_type` tinyint(4) DEFAULT NULL COMMENT 'Loại căn hộ, cao cấp, văn phòng, bình dân, ',
  `furniture_type` tinyint(4) DEFAULT NULL COMMENT 'Loại nội thất, cơ bản: 0, đầy đủ: 1, chưa biết: 3.',
  `price_rent` int(20) DEFAULT NULL COMMENT 'giá cho thuê',
  `return_rate` double(3,2) DEFAULT NULL COMMENT 'tỷ suất sinh lời',
  `legal_doc` int(11) DEFAULT NULL COMMENT 'Giấy tờ pháp lý',
  `description` varchar(2000) DEFAULT NULL COMMENT 'Mô tả chi tiết bđs',
  `width_y` int(6) DEFAULT NULL COMMENT 'chiều rộng',
  `long_x` int(6) DEFAULT NULL COMMENT 'chiều dài',
  `street_house` tinyint(1) DEFAULT NULL COMMENT 'nhà có phải mặt đường hay không?',
  `FSBO` tinyint(1) DEFAULT NULL COMMENT 'Nhà đăng bởi chủ sở hữu hay không?',
  `view_num` int(11) DEFAULT NULL COMMENT 'Số lượt xem',
  `create_by` int(12) DEFAULT NULL COMMENT 'Tạo bởi ai, có thể là chính khách hàng hoặc nhân viên môi giới',
  `update_by` int(12) DEFAULT NULL COMMENT 'Sửa bởi ai, có thể là chính khách hàng hoặc nhân viên môi giới',
  `shape` varchar(200) DEFAULT NULL COMMENT 'Hình dáng ngôi nhà \r\nKhá cân đối \r\nCân đối',
  `distance2facade` int(12) DEFAULT NULL COMMENT 'Khoảng cách ra mặt tiền đường (m)',
  `adjacent_facade_num` int(2) DEFAULT NULL COMMENT 'Số mặt tiền tiếp giáp',
  `adjacent_road` varchar(200) DEFAULT NULL COMMENT 'Đường tiếp giáp Bê tông',
  `alley_min_width` int(6) DEFAULT NULL COMMENT 'Độ rộng đường hẻm nhỏ nhất (m)',
  `adjacent_alley_min_width` int(6) DEFAULT NULL COMMENT 'Độ rộng đường hẻm tiếp giáp (m)',
  `factor` int(10) DEFAULT NULL COMMENT 'Yếu tố khác: Gần chợ',
  `structure` varchar(2000) DEFAULT NULL COMMENT 'Kết cấu:\r\nTường gạch, sàn BTCT, mái BTCT + tôn ',
  `DTSXD` int(6) UNSIGNED DEFAULT NULL COMMENT 'DTSXD (m2)',
  `CLCL` int(2) UNSIGNED DEFAULT NULL COMMENT 'CLCL (%)',
  `CTXD_price` int(12) DEFAULT 0 COMMENT 'Đơn giá CTXD (đồng/m2)',
  `CTXD_value` int(12) DEFAULT 0 COMMENT 'Giá trị CTXD (đồng)',
  `photo` varchar(2000) DEFAULT '' COMMENT 'Lưu thông tin đường dẫn ảnh và video',
  `_lat` double DEFAULT NULL COMMENT 'Vĩ độ',
  `_lng` double DEFAULT NULL COMMENT 'Kinh độ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Bảng lưu thông tin bđs muốn bán.';
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

// generate java entity base on this

@Entity
@Table(name = "realestate")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RealEstate {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne
  @JoinColumn(name = "type_id")
  private RealEstateType type;

  @Column(name = "title", length = 2000)
  private String title;

  @ManyToOne
  @JoinColumn(name = "request_id")
  private Request request;

  @ManyToOne
  @JoinColumn(name = "province_id")
  private Province province;

  @ManyToOne
  @JoinColumn(name = "district_id")
  private District district;

  @ManyToOne
  @JoinColumn(name = "ward_id")
  private Ward ward;

  @ManyToOne
  @JoinColumn(name = "street_id")
  private Street street;

  @ManyToOne
  @JoinColumn(name = "project_id")
  private Project project;

  @Column(name = "address", length = 2000)
  private String address;

  @ManyToOne
  @JoinColumn(name = "customer_id")
  private Customer customer;

  @Column(name = "price")
  private int price;

  @Column(name = "price_min")
  private int priceMin;

  @ManyToOne
  @JoinColumn(name = "price_time_id")
  private PriceTime priceTime;

  @Column(name = "date_create")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date dateCreate;

  @Column(name = "acreage")
  private double acreage;

  @ManyToOne
  @JoinColumn(name = "direction_id")
  private Direction direction;

  @Column(name = "total_floor")
  private int totalFloor;

  @Column(name = "number_floor")
  private int numberFloor;

  @Column(name = "bath")
  private int bath;

  @Column(name = "apart_code")
  private String apartCode;

  @Column(name = "wall_area")
  private double wallArea;

  @Column(name = "bedroom")
  private int bedroom;

  @Column(name = "balcony")
  private int balcony;

  @ManyToOne
  @JoinColumn(name = "landscape_view_id")
  private LandScapeView landscapeView;

  @ManyToOne
  @JoinColumn(name = "apart_loca_id")
  private ApartLoca apartLoca;

  @ManyToOne
  @JoinColumn(name = "apart_type_id")
  private ApartType apartType;

  @ManyToOne
  @JoinColumn(name = "furniture_type_id")
  private FurnitureType furnitureType;

  @Column(name = "price_rent")
  private int priceRent;

  @Column(name = "return_rate")
  private double returnRate;

  private int legalDoc;

  @Column(name = "description", length = 2000)
  private String description;

  @Column(name = "width_y")
  private int widthY;

  @Column(name = "long_x")
  private int longX;

  @Column(name = "street_house")
  private boolean streetHouse;

  @Column(name = "FSBO")
  private boolean FSBO;

  @Column(name = "view_num")
  private int viewNum;

  @Column(name = "create_by")
  private int createBy;

  @Column(name = "update_by")
  private int updateBy;

  @ManyToOne
  @JoinColumn(name = "shape_id")
  private Shape shape;

  @Column(name = "distance2facade")
  private int distance2facade;

  @Column(name = "adjacent_facade_num")
  private int adjacentFacadeNum;

  @Column(name = "adjacent_road")
  private String adjacentRoad;

  @Column(name = "alley_min_width")
  private int alleyMinWidth;

  @Column(name = "adjacent_alley_min_width")
  private int adjacentAlleyMinWidth;

  @ManyToOne
  @JoinColumn(name = "factor_id")
  private Factor factor;

  @Column(name = "structure")
  private String structure;

  @Column(name = "DTSXD")
  private int DTSXD;

  @Column(name = "CLCL")
  private int CLCL;

  @Column(name = "CTXD_price")
  private int CTXDPrice;

  @Column(name = "CTXD_value")
  private int CTXDValue;

  @Column(name = "photo")
  private String photo;

  @Column(name = "_lat")
  private double lat;

  @Column(name = "_lng")
  private double lng;
}
