package com.devcamp.realestateexchangev1.Entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `project` (
  `id` int(10) UNSIGNED NOT NULL,
  `_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_province_id` int(10) UNSIGNED DEFAULT NULL,
  `_district_id` int(10) UNSIGNED DEFAULT NULL,
  `_ward_id` int(10) DEFAULT NULL COMMENT 'id của phường',
  `_street_id` int(10) DEFAULT NULL COMMENT 'id của đường/phố',
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Địa chỉ chung cư.',
  `slogan` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Slogan(phương châm) của dự án.',
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Mô tả, giới thiệu chung dự án, ',
  `acreage` decimal(20,2) DEFAULT NULL COMMENT 'Tổng diện tích dự án',
  `construct_area` decimal(20,2) DEFAULT NULL COMMENT 'Tổng diện tích xây dựng của dự án',
  `num_block` smallint(6) DEFAULT NULL COMMENT 'số block dự án có',
  `num_floors` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Số tầng, có thể là khoảng tương đối nên có kiểu text (20-38)',
  `num_apartment` int(11) NOT NULL COMMENT 'tổng số căn hộ.',
  `apartmentt_area` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'diện tích căn hộ, có thể là khoảng',
  `investor` int(11) NOT NULL COMMENT 'Chủ đầu tư.',
  `construction_contractor` int(11) DEFAULT NULL COMMENT 'Construction contractor nhà thầu xây dựng',
  `design unit` int(11) DEFAULT NULL COMMENT 'đơn vị thiết kế dự án',
  `utilities` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tiện ích chung cư.',
  `region_link` varchar(1000) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Vị trí liên Kết vùng',
  `photo` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_lat` double UNSIGNED DEFAULT NULL,
  `_lng` double UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

// generate java entity base on this

@Data
@Entity
@Table(name = "project")
@NoArgsConstructor
@AllArgsConstructor
public class Project {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name", nullable = false)
  @NotBlank(message = "Tên dự án không được để trống")
  private String name;

  @ManyToOne
  @JoinColumn(name = "_province_id", nullable = true)
  private Province province;

  @ManyToOne
  @JoinColumn(name = "_district_id", nullable = true)
  private District district;

  @ManyToOne
  @JoinColumn(name = "_ward_id", nullable = true)
  private Ward ward;

  @ManyToOne
  @JoinColumn(name = "_street_id", nullable = true)
  private Street street;

  @Column(nullable = true)
  private String address;

  @Column(nullable = true)
  private String slogan;

  @Column(nullable = true, length = 4000)
  private String description;

  @Column(nullable = true)
  private Double acreage;

  @Column(nullable = true)
  private Double constructArea;

  @Column(nullable = true)
  private Integer numBlock;

  @Column(nullable = true)
  private String numFloors;

  @Column(nullable = true)
  private Integer numApartment;

  @Column(nullable = true)
  private String apartmenttArea;

  @ManyToOne
  @JoinColumn(name = "investor", nullable = true)
  private Investor investor;

  @ManyToOne
  @JoinColumn(name = "construction_contractor", nullable = true)
  private ConstructionContractor constructionContractor;

  @ManyToOne
  @JoinColumn(name = "design_unit", nullable = true)
  private DesignUnit designUnit;

  @Column(nullable = true)
  private String utilities;

  @Column(nullable = true)
  private String regionLink;

  // @JoinColumn(name = "utilities", nullable = true)
  // private List<Utility> utilities;

  // @JoinColumn(name = "region_link", nullable = true)
  // private List<RegionLink> regionLinks;

  @Column(nullable = true)
  private String photo;

  @Lob
  @Column(nullable = true)
  private byte[] image;

  @Column(name = "_lat", nullable = true)
  private Double lat;

  @Column(name = "_lng", nullable = true)
  private Double lng;

}
