package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.CascadeType;

//CREATE TABLE `construction_contractor` (
//     `id` int(11) NOT NULL,
//     `name` varchar(1000) NOT NULL COMMENT 'Tên nhà thầu xây dựng dự án.',
//     `description` varchar(5000) DEFAULT NULL COMMENT 'Mô tả chung nhà thầu xây dựng',
//     `projects` varchar(2000) DEFAULT NULL COMMENT 'Những dự án mà nhà thầu xây dựng làm',
//     `address` int(11) DEFAULT NULL COMMENT 'địa chỉ nhà thầu xây dựng',
//     `phone` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại nhà thầu xây dựng',
//     `phone2` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại2 nhà thầu xây dựng',
//     `fax` varchar(50) DEFAULT NULL COMMENT 'Số fax nhà thầu xây dựng',
//     `email` varchar(200) DEFAULT NULL,
//     `website` varchar(1000) DEFAULT NULL,
//     `note` mediumtext DEFAULT NULL COMMENT 'Thông tin khác về nhà thầu xây dựng.'
//   ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Thông tin nhà thầu xây dựng dự án';

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.engine.internal.ForeignKeys;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "construction_contractor")
public class ConstructionContractor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "Tên nhà thầu không được để trống")
    private String name;

    @Column(nullable = true)
    private String description;

    @Column(nullable = true)
    private String projects;

    // @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    // @JoinColumn(name = "address", nullable = true)
    // private AddressMap address;

    // field AddressMap, set null when AddressMap is deleted
    @ManyToOne
    @JoinColumn(name = "address", nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private AddressMap address;
    

    @Column(nullable = true)
    private String phone;

    @Column(nullable = true)
    private String phone2;

    @Column(nullable = true)
    private String fax;

    @Column(nullable = true)
    private String email;

    @Column(nullable = true)
    private String website;

    @Column(nullable = true)
    private String note;
}
