package com.devcamp.realestateexchangev1.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

// generate java entity base on this
/*
 * CREATE TABLE `employees` (
  `EmployeeID` int(11) NOT NULL,
  `LastName` varchar(20) NOT NULL,
  `FirstName` varchar(10) NOT NULL,
  `Title` varchar(30) DEFAULT NULL,
  `TitleOfCourtesy` varchar(25) DEFAULT NULL,
  `BirthDate` datetime DEFAULT NULL,
  `HireDate` datetime DEFAULT NULL,
  `Address` varchar(60) DEFAULT NULL,
  `City` varchar(15) DEFAULT NULL,
  `Region` varchar(15) DEFAULT NULL,
  `PostalCode` varchar(10) DEFAULT NULL,
  `Country` varchar(15) DEFAULT NULL,
  `HomePhone` varchar(24) DEFAULT NULL,
  `Extension` varchar(4) DEFAULT NULL,
  `Photo` varchar(50) DEFAULT NULL,
  `Notes` longtext DEFAULT NULL,
  `ReportsTo` int(11) DEFAULT NULL,
  `Username` varchar(50) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Activated` enum('Y','N') DEFAULT NULL,
  `Profile` longtext DEFAULT NULL,
  `UserLevel` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */

@Data
@Entity
@Table(name = "employees")
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "EmployeeID")
  private int id;

  @Column(name = "LastName")
  @NotBlank(message = "LastName không được để trống")
  private String lastName;

  @Column(name = "FirstName")
  @NotBlank(message = "FirstName không được để trống")
  private String firstName;

  @Column(name = "Title")
  private String title;

  @Column(name = "TitleOfCourtesy")
  private String titleOfCourtesy;

  @Column(name = "BirthDate")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date birthDate;

  @Column(name = "HireDate")
  @JsonFormat(pattern = "yyyy-MM-dd")
  private Date hireDate;

  @Column(name = "Address")
  private String address;

  @Column(name = "City")
  private String city;

  @Column(name = "Region")
  private String region;

  @Column(name = "PostalCode")
  private String postalCode;

  @Column(name = "Country")
  private String country;

  @Column(name = "HomePhone")
  private String homePhone;

  @Column(name = "Extension")
  private String extension;

  @Column(name = "Photo")
  private String photo;

  @Column(name = "Notes")
  private String notes;

  @Column(name = "ReportsTo")
  private Integer reportsTo;

  @Column(name = "Username")
  private String username;

  @Column(name = "Password")
  private String password;

  @Column(name = "Email")
  private String email;

  @Column(name = "Activated")
  private String activated;

  @Column(name = "Profile")
  private String profile;

  @Column(name = "UserLevel")
  private Integer userLevel;
}
