package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `endpoint` longtext NOT NULL,
  `publickey` varchar(255) NOT NULL,
  `authenticationtoken` varchar(255) NOT NULL,
  `contentencoding` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

// generate java entity base on this

@Data
@Entity
@Table(name = "subscriptions")
@NoArgsConstructor
@AllArgsConstructor
public class Subscription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user")
    private String user;

    @Column(name = "endpoint")
    private String endpoint;

    @Column(name = "publickey")
    private String publickey;

    @Column(name = "authenticationtoken")
    private String authenticationtoken;

    @Column(name = "contentencoding")
    private String contentencoding;

}
