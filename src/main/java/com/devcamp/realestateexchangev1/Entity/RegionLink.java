package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `region_link` (
 * `id` int(11) NOT NULL,
 * `name` varchar(1000) NOT NULL COMMENT 'Tên địa điểm kết nối',
 * `description` text DEFAULT NULL COMMENT 'mô tả địa điểm kết nối',
 * `photo` varchar(5000) DEFAULT NULL,
 * `address` varchar(5000) DEFAULT NULL COMMENT 'địa chỉ địa điểm kết nối',
 * `_lat` double DEFAULT NULL COMMENT 'Latitude vĩ độ',
 * `_lng` double DEFAULT NULL COMMENT 'Longitude kinh độ'
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='địa lợi kết nối khu vực
 * (chợ, trường, bệnh viện, cao tốc)';
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

// generate java entity base on this

@Data
@Entity
@Table(name = "region_link")
@NoArgsConstructor
@AllArgsConstructor
public class RegionLink {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "Tên địa điểm kết nối không được để trống")
    private String name;

    @Column(nullable = true)
    private String description;

    @Column(nullable = true)
    private String photo;

    @Column(nullable = true)
    private String address;

    @Column(name = "_lat", nullable = true)
    private Double lat;

    @Column(name = "_lng", nullable = true)
    private Double lng;

}