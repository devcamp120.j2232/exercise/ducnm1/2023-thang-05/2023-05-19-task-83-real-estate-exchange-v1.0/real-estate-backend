package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `street` (
  `id` int(10) UNSIGNED NOT NULL,
  `_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_prefix` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_province_id` int(10) UNSIGNED DEFAULT NULL,
  `_district_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

// generate java entity base on this

@Data
@Entity
@Table(name = "street")
@NoArgsConstructor
@AllArgsConstructor
public class Street {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "_name")
    @NotBlank(message = "Tên đường không được để trống")
    private String name;

    @Column(name = "_prefix")
    private String prefix;

    // this is mapped by province class
    @ManyToOne
    @JoinColumn(name = "_province_id")
    private Province province;

    // this is mapped by district class
    @ManyToOne
    @JoinColumn(name = "_district_id")
    private District district;
}