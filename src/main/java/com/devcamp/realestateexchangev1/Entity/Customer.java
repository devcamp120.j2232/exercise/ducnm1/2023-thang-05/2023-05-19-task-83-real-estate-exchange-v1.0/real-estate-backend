package com.devcamp.realestateexchangev1.Entity;

import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

/*
 * CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_title` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `mobile` varchar(80) DEFAULT NULL,
  `email` varchar(24) DEFAULT NULL,
  `note` varchar(5000) DEFAULT NULL COMMENT 'Note thông tin khách hàng',
  `create_by` int(11) DEFAULT NULL COMMENT 'thông tin người tạo',
  `update_by` int(11) DEFAULT NULL COMMENT 'Thông tin người sửa',
  `create_date` datetime DEFAULT NULL COMMENT 'ngày tạo',
  `update_date` datetime DEFAULT NULL COMMENT 'ngày sửa cuối'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 */
// generate java entity base on this

@Data
@Entity
@Table(name = "customers")
@NoArgsConstructor
@AllArgsConstructor
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "contact_name")
  private String contactName;

  @Column(name = "contact_title")
  private String contactTitle;

  private String address;

  private String mobile;

  private String email;

  @Column(length = 5000)
  private String note;

  @Column(name = "create_by")
  private Integer createBy;

  @Column(name = "update_by")
  private Integer updateBy;

  @Column(name = "create_date")
  @JsonFormat(pattern = "yyyy-MM-dd")
  @Temporal(TemporalType.TIMESTAMP)
  private Date createDate;

  @Column(name = "update_date")
  @JsonFormat(pattern = "yyyy-MM-dd")
  @Temporal(TemporalType.TIMESTAMP)
  private Date updateDate;
}
