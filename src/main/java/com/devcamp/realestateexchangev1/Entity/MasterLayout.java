package com.devcamp.realestateexchangev1.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `master_layout` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `description` varchar(5000) DEFAULT NULL COMMENT 'mô tả chung',
  `project_id` int(11) NOT NULL COMMENT 'thuộc dự án',
  `acreage` decimal(12,2) DEFAULT NULL COMMENT 'diện tích tổng',
  `apartment_list` varchar(1000) DEFAULT NULL COMMENT 'Danh sách căn hộ điển hình của mặt bằng, dùng để copy cho các căn hộ khác.',
  `photo` varchar(5000) DEFAULT NULL COMMENT 'Ảnh mặt bằng điển hình',
  `date_create` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'Ngày tạo',
  `date_update` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'Ngày cập nhật'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Mặt Bằng điển hình mỗi tầng của các tòa nhà trong dự án';
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

// generate java entity base on this

@Data
@Entity
@Table(name = "master_layout")
@NoArgsConstructor
@AllArgsConstructor
public class MasterLayout {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "Tên không được để trống")
    private String name;

    @Column(nullable = true)
    private String description;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Transient
    private String projectName;

    @Column(nullable = true)
    private Double acreage;

    @Column(nullable = true)
    private String apartment_list;

    @Column(nullable = true)
    private String photo;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date_create;

    @Column(nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date_update;

    // getter setter for projectName
    public String getProjectName() {
        return this.project.getName();
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
