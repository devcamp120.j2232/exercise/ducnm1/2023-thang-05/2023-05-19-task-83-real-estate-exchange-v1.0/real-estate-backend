package com.devcamp.realestateexchangev1.Entity;

public enum ERole {
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN
}
