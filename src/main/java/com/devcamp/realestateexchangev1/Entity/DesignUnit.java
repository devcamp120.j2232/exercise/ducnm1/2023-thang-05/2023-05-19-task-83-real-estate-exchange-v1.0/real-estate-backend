package com.devcamp.realestateexchangev1.Entity;

/*
 * CREATE TABLE `design unit` (
  `id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL COMMENT 'Tên đơn vị thiết kế dự án dự án.',
  `description` varchar(5000) DEFAULT NULL COMMENT 'Mô tả chung đơn vị thiết kế dự án',
  `projects` varchar(2000) DEFAULT NULL COMMENT 'Những dự án mà đơn vị thiết kế dự án làm',
  `address` int(11) DEFAULT NULL COMMENT 'địa chỉ đơn vị thiết kế dự án',
  `phone` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại đơn vị thiết kế dự án',
  `phone2` varchar(50) DEFAULT NULL COMMENT 'Số điện thoại2 đơn vị thiết kế dự án',
  `fax` varchar(50) DEFAULT NULL COMMENT 'Số fax đơn vị thiết kế dự án',
  `email` varchar(200) DEFAULT NULL,
  `website` varchar(1000) DEFAULT NULL,
  `note` mediumtext DEFAULT NULL COMMENT 'Thông tin khác về đơn vị thiết kế dự án.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Thông tin đơn vị thiết kế dự án';
 */

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//generate entity base on this
@Data
@Entity
@Table(name = "design_unit")
@NoArgsConstructor
@AllArgsConstructor
public class DesignUnit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false)
    @NotBlank(message = "Tên đơn vị không được để trống")
    private String name;
    
    @Column(nullable = true)
    private String description;

    @Column(nullable = true)
    private String projects;

    @ManyToOne
    @JoinColumn(name = "address", nullable = true)
    private AddressMap address;

    @Column(nullable = true)
    private String phone;

    @Column(nullable = true)
    private String phone2;

    @Column(nullable = true)
    private String fax;

    @Column(nullable = true)
    private String email;

    @Column(nullable = true)
    private String website;

    @Column(nullable = true)
    private String note;
}

