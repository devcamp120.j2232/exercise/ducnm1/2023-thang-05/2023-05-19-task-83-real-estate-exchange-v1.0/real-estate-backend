package com.devcamp.realestateexchangev1.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;

/*
 * CREATE TABLE `province` (
  `id` int(10) UNSIGNED NOT NULL,
  `_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
 */

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

// generate java entity base on this

@Data
@Entity
@Table(name = "province")
@NoArgsConstructor
@AllArgsConstructor
public class Province {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "_name")
  @NotBlank(message = "Tên tỉnh/thành phố không được để trống")
  private String name;

  @Column(name = "_code")
  @NotBlank(message = "Mã tỉnh/thành phố không được để trống")
  private String code;

}
