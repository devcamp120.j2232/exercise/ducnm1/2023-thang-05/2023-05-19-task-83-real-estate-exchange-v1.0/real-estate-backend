package com.devcamp.realestateexchangev1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Realestateexchangev1Application {

	// @Autowired
	// RoleRepository roleRepository;

	// @Autowired
	// UserRepository userRepository;

	// @Autowired
	// PasswordEncoder encoder;

	public static void main(String[] args) {
		SpringApplication.run(Realestateexchangev1Application.class, args);
	}

	// @Override
	// public void run(String... params) throws Exception {

	// 	if (!roleRepository.findByName(ERole.ROLE_USER).isPresent()) {
	// 		roleRepository.save(new Role(ERole.ROLE_USER));
	// 	}

	// 	if (!roleRepository.findByName(ERole.ROLE_MODERATOR).isPresent()) {
	// 		roleRepository.save(new Role(ERole.ROLE_MODERATOR));
	// 	}

	// 	if (!roleRepository.findByName(ERole.ROLE_ADMIN).isPresent()) {
	// 		roleRepository.save(new Role(ERole.ROLE_ADMIN));
	// 	}
	// }

}
