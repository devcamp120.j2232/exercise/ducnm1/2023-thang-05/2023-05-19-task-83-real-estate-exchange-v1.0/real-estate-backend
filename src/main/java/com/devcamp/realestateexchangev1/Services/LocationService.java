package com.devcamp.realestateexchangev1.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Location;
import com.devcamp.realestateexchangev1.Repositories.ILocationRepository;

@Service
public class LocationService {
    @Autowired
    private ILocationRepository locationRepository;

    // get all location
    public List<Location> getAllLocation() {
        return locationRepository.findAll();
    }

    // get location by id
    public Location getLocationById(int id) {
        return locationRepository.findById(id).get();
    }

    // create a new location
    public Location createLocation(Location location){
        return locationRepository.save(location);
    }

    // update location
    public Location updateLocation(int id, Location location){
        Location locationUpdate = locationRepository.findById(id).get();

        locationUpdate.setLatitude(location.getLatitude());
        locationUpdate.setLongitude(location.getLongitude());

        return locationRepository.save(locationUpdate);
    }

    // delete location by id
    public String deleteLocation(int id){
        locationRepository.deleteById(id);
        return "Location with id: " + id + " has been deleted!";
    }

    // delete all location
    public String deleteAllLocation(){
        locationRepository.deleteAll();
        return "All Location has been deleted!";
    }

    

}
