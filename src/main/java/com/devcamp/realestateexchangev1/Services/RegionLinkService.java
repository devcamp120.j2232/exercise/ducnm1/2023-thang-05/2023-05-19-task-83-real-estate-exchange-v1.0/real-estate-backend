package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.RegionLink;
import com.devcamp.realestateexchangev1.Repositories.IRegionLinkRepository;

import javassist.NotFoundException;

@Service
public class RegionLinkService {
    @Autowired
    private IRegionLinkRepository regionLinkRepository;

    // get all region link
    public List<RegionLink> getAllRegionLink() {
        return regionLinkRepository.findAll();
    }

    // get region link by id
    public RegionLink getRegionLinkById(int id) {
        return regionLinkRepository.findById(id).get();
    }

    // create region link
    public RegionLink createRegionLink(RegionLink regionLink) {
        return regionLinkRepository.save(regionLink);
    }

    // update region link
    /*
     * public class RegionLink {
     * 
     * @Id
     * 
     * @GeneratedValue(strategy = GenerationType.IDENTITY)
     * private int id;
     * 
     * @Column(nullable = false)
     * private String name;
     * 
     * @Column(nullable = true)
     * private String description;
     * 
     * @Column(nullable = true)
     * private String photo;
     * 
     * @Column(nullable = true)
     * private String address;
     * 
     * @Column(name = "_lat", nullable = true)
     * private double lat;
     * 
     * @Column(name = "_lng", nullable = true)
     * private double lng;
     * 
     * }
     */
    public RegionLink updateRegionLink(int id, RegionLink regionLink) throws NotFoundException {
        Optional<RegionLink> optionalRegionLink = regionLinkRepository.findById(id);
        if (!optionalRegionLink.isPresent()) {
            throw new NotFoundException("RegionLink not found with id " + id);
        }

        RegionLink foundRegionLink = optionalRegionLink.get();
        foundRegionLink.setName(regionLink.getName());
        foundRegionLink.setDescription(regionLink.getDescription());
        foundRegionLink.setPhoto(regionLink.getPhoto());
        foundRegionLink.setAddress(regionLink.getAddress());
        foundRegionLink.setLat(regionLink.getLat());
        foundRegionLink.setLng(regionLink.getLng());
        return regionLinkRepository.save(foundRegionLink);
    }

    // delete region link
    public String deleteRegionLink(int id) throws NotFoundException {
        Optional<RegionLink> optionalRegionLink = regionLinkRepository.findById(id);
        if (!optionalRegionLink.isPresent()) {
            throw new NotFoundException("RegionLink not found with id " + id);
        }
        regionLinkRepository.deleteById(id);
        return "Delete RegionLink with id " + id + " success!";
    }

    // delete all region link
    public String deleteAllRegionLink() {
        regionLinkRepository.deleteAll();
        return "Delete all RegionLink success!";
    }

}
