package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Ward;
import com.devcamp.realestateexchangev1.Repositories.IWardRepository;

import javassist.NotFoundException;

@Service
public class WardService {
    @Autowired
    private IWardRepository wardRepository;

    // get all ward
    public List<Ward> getAllWard() {
        return wardRepository.findAll();
    }

    // get ward by id
    public Ward getWardById(int id) {
        return wardRepository.findById(id).get();
    }

    // get ward by district id
    public List<Ward> getWardByDistrictId(int districtId) {
        return wardRepository.findByDistrictId(districtId);
    }

    // create ward
    public Ward createWard(Ward ward) {
        return wardRepository.save(ward);
    }

    public Ward updateWard(int id, Ward ward) throws NotFoundException {
        Optional<Ward> optionalWard = wardRepository.findById(id);
        if (!optionalWard.isPresent()) {
            throw new NotFoundException("Ward not found with id " + id);
        }

        Ward foundWard = optionalWard.get();
        foundWard.setName(ward.getName());
        foundWard.setPrefix(ward.getPrefix());
        foundWard.setProvince(ward.getProvince());
        foundWard.setDistrict(ward.getDistrict());
        return wardRepository.save(foundWard);
    }

    // delete ward return string
    public String deleteWard(int id) throws NotFoundException {
        Optional<Ward> optionalWard = wardRepository.findById(id);
        if (!optionalWard.isPresent()) {
            throw new NotFoundException("Ward not found with id " + id);
        }
        wardRepository.deleteById(id);
        return "Ward with " + id + " deleted successfully";
    }

    // delete all ward return stirng
    public String deleteAllWard() {
        wardRepository.deleteAll();
        return "All ward deleted successfully";
    }

}
