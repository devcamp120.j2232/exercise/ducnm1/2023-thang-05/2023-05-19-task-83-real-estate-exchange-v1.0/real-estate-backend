package com.devcamp.realestateexchangev1.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.District;
import com.devcamp.realestateexchangev1.Repositories.IDistrictRepository;

@Service
public class DistrictService {
    @Autowired
    private IDistrictRepository districtRepository;

    // get all district
    public List<District> getAllDistrict() {
        return districtRepository.findAll();
    }

    // get district by id
    public District getDistrictById(int id) {
        return districtRepository.findById(id).get();
    }

    public List<District> getDistrictByProvinceId(int provinceId) {
        return districtRepository.findByProvinceId(provinceId);
    }

    // create district
    public District createDistrict(District district) {
        return districtRepository.save(district);
    }

    // update district by id
    public District updateDistrict(int id, District district) {
        District districtUpdate = districtRepository.findById(id).get();

        districtUpdate.setName(district.getName());
        districtUpdate.setPrefix(district.getPrefix());
        districtUpdate.setProvince(district.getProvince());

        return districtRepository.save(districtUpdate);
    }

    // delete district by id
    public String deleteDistrict(int id) {
        districtRepository.deleteById(id);
        return "District with id " + id + " deleted successfully";
    }

    // delete all district
    public String deleteAllDistrict() {
        districtRepository.deleteAll();
        return "All District deleted successfully";
    }
}
