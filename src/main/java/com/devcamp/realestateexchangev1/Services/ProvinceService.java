package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Province;
import com.devcamp.realestateexchangev1.Repositories.IProvinceRepository;

import javassist.NotFoundException;

@Service
public class ProvinceService {
    @Autowired
    private IProvinceRepository provinceRepository;

    // get all province
    public List<Province> getAllProvince() {
        return provinceRepository.findAll();
    }

    // get province by id
    public Province getProvinceById(int id) {
        return provinceRepository.findById(id).get();
    }

    // create province
    public Province createProvince(Province province) {
        return provinceRepository.save(province);
    }

    // update province
    /*
     * public class Province {
     * 
     * @Id
     * 
     * @GeneratedValue(strategy = GenerationType.IDENTITY)
     * private int id;
     * 
     * @Column(name = "_name")
     * private String name;
     * 
     * @Column(name = "_code")
     * private String code;
     * 
     * }
     */
    public Province updateProvince(int id, Province province) throws NotFoundException {
        Optional<Province> optionalProvince = provinceRepository.findById(id);
        if (!optionalProvince.isPresent()) {
            throw new NotFoundException("Province not found with id " + id);
        }

        Province foundProvince = optionalProvince.get();
        foundProvince.setName(province.getName());
        foundProvince.setCode(province.getCode());
        return provinceRepository.save(foundProvince);
    }

    // delete province
    public String deleteProvince(int id) throws NotFoundException {
        Optional<Province> optionalProvince = provinceRepository.findById(id);
        if (!optionalProvince.isPresent()) {
            throw new NotFoundException("Province not found with id " + id);
        }

        provinceRepository.deleteById(id);
        return "Province with id " + id + " has been deleted";
    }

    // deleete all province
    public String deleteAllProvince() {
        provinceRepository.deleteAll();
        return "All province has been deleted";
    }

}
