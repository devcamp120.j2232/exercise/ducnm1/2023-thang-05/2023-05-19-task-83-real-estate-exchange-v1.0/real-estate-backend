package com.devcamp.realestateexchangev1.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Employee;
import com.devcamp.realestateexchangev1.Repositories.IEmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    private IEmployeeRepository employeeRepository;

    // get all employee
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    // get employee by id
    public Employee getEmployeeById(int id) {
        return employeeRepository.findById(id).get();
    }

    // create employee
    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    // update employee
     public Employee updateEmployee(int id, Employee employee) {
        Employee employeeUpdate = employeeRepository.findById(id).get();

        employeeUpdate.setLastName(employee.getLastName());
        employeeUpdate.setFirstName(employee.getFirstName());
        employeeUpdate.setTitle(employee.getTitle());
        employeeUpdate.setTitleOfCourtesy(employee.getTitleOfCourtesy());
        employeeUpdate.setBirthDate(employee.getBirthDate());
        employeeUpdate.setHireDate(employee.getHireDate());
        employeeUpdate.setAddress(employee.getAddress());
        employeeUpdate.setCity(employee.getCity());
        employeeUpdate.setRegion(employee.getRegion());
        employeeUpdate.setPostalCode(employee.getPostalCode());
        employeeUpdate.setCountry(employee.getCountry());
        employeeUpdate.setHomePhone(employee.getHomePhone());
        employeeUpdate.setExtension(employee.getExtension());
        employeeUpdate.setPhoto(employee.getPhoto());
        employeeUpdate.setNotes(employee.getNotes());
        employeeUpdate.setReportsTo(employee.getReportsTo());
        employeeUpdate.setUsername(employee.getUsername());
        employeeUpdate.setPassword(employee.getPassword());
        employeeUpdate.setEmail(employee.getEmail());
        employeeUpdate.setActivated(employee.getActivated());
        employeeUpdate.setProfile(employee.getProfile());
        employeeUpdate.setUserLevel(employee.getUserLevel());

        return employeeRepository.save(employeeUpdate);
     }

    // delete employee by id
    public String deleteEmployee(int id) {
        employeeRepository.deleteById(id);
        return "Employee with id " + id + " deleted successfully";
    }

    // delete all employee
    public String deleteAllEmployee() {
        employeeRepository.deleteAll();
        return "All Employee deleted successfully";
    }

}
