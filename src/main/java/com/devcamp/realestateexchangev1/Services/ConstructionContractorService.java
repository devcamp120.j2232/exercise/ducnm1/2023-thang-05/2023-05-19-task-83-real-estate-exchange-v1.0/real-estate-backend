package com.devcamp.realestateexchangev1.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.ConstructionContractor;
import com.devcamp.realestateexchangev1.Repositories.IConstructionContractorRepository;

import javassist.NotFoundException;

@Service
public class ConstructionContractorService {
    @Autowired
    private IConstructionContractorRepository constructionContractorRepository;

    @Autowired
    private AddressMapService addressMapService;

    // get all construction contractor
    public List<ConstructionContractor> getAllConstructionContractor() {
        return constructionContractorRepository.findAll();
    }

    // get construction contractor by id
    public ConstructionContractor getConstructionContractorById(int id) {
        return constructionContractorRepository.findById(id).get();
    }

    // create construction contractor
    public ConstructionContractor createConstructionContractor(ConstructionContractor constructionContractor)
            throws NotFoundException {
        ConstructionContractor newConstructionContractor = new ConstructionContractor();

        newConstructionContractor.setName(constructionContractor.getName());
        newConstructionContractor.setDescription(constructionContractor.getDescription());
        newConstructionContractor.setProjects(constructionContractor.getProjects());
        newConstructionContractor
                .setAddress(addressMapService.getAddressMapById(constructionContractor.getAddress().getId()));
        newConstructionContractor.setPhone(constructionContractor.getPhone());
        newConstructionContractor.setPhone2(constructionContractor.getPhone2());
        newConstructionContractor.setFax(constructionContractor.getFax());
        newConstructionContractor.setEmail(constructionContractor.getEmail());
        newConstructionContractor.setWebsite(constructionContractor.getWebsite());
        newConstructionContractor.setNote(constructionContractor.getNote());

        return constructionContractorRepository.save(constructionContractor);
    }

    // update construction contractor
    public ConstructionContractor updateConstructionContractor(int id, ConstructionContractor constructionContractor) throws NotFoundException {
        ConstructionContractor constructionContractorUpdate = constructionContractorRepository.findById(id).get();

        constructionContractorUpdate.setName(constructionContractor.getName());
        constructionContractorUpdate.setDescription(constructionContractor.getDescription());
        constructionContractorUpdate.setProjects(constructionContractor.getProjects());
        constructionContractorUpdate
                .setAddress(addressMapService.getAddressMapById(constructionContractor.getAddress().getId()));
        constructionContractorUpdate.setPhone(constructionContractor.getPhone());
        constructionContractorUpdate.setPhone2(constructionContractor.getPhone2());
        constructionContractorUpdate.setFax(constructionContractor.getFax());
        constructionContractorUpdate.setEmail(constructionContractor.getEmail());
        constructionContractorUpdate.setWebsite(constructionContractor.getWebsite());
        constructionContractorUpdate.setNote(constructionContractor.getNote());

        return constructionContractorRepository.save(constructionContractorUpdate);
    }

    // delete construction contractor
    public String deleteConstructionContractor(int id) {
        constructionContractorRepository.deleteById(id);
        return "ConstructionContractor with id " + id + " deleted successfully";
    }

    // delete all construction contractor
    public String deleteAllConstructionContractor() {
        constructionContractorRepository.deleteAll();
        return "All ConstructionContractor deleted successfully";
    }
}
