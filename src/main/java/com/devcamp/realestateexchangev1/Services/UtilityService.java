package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Utility;
import com.devcamp.realestateexchangev1.Repositories.IUtilityRepository;

import javassist.NotFoundException;

@Service
public class UtilityService {
    @Autowired
    private IUtilityRepository utilityRepository;

    // get all utility
    public List<Utility> getAllUtility() {
        return utilityRepository.findAll();
    }

    // get utility by id
    public Utility getUtilityById(int id) {
        return utilityRepository.findById(id).get();
    }

    // create utility
    public Utility createUtility(Utility utility) {
        return utilityRepository.save(utility);
    }

    // update utility
    /*
     * public class Utility {
     * 
     * @Id
     * 
     * @GeneratedValue(strategy = GenerationType.IDENTITY)
     * private int id;
     * 
     * @Column(nullable = false)
     * private String name;
     * 
     * @Column(nullable = true)
     * private String description;
     * 
     * @Column(nullable = true)
     * private String photo;
     * }
     */
    public Utility updateUtility(int id, Utility utility) throws NotFoundException {
        Optional<Utility> utilityData = utilityRepository.findById(id);
        if (utilityData.isPresent()) {
            Utility utilityUpdate = utilityData.get();
            utilityUpdate.setName(utility.getName());
            utilityUpdate.setDescription(utility.getDescription());
            utilityUpdate.setPhoto(utility.getPhoto());
            return utilityRepository.save(utilityUpdate);
        } else {
            throw new NotFoundException("Utility not found with id: " + id);
        }
    }

    // delete utility
    public String deleteUtility(int id) throws NotFoundException {
        Optional<Utility> utilityData = utilityRepository.findById(id);
        if (utilityData.isPresent()) {
            utilityRepository.deleteById(id);
            return "Utility with id " + id + " has been deleted!";
        } else {
            throw new NotFoundException("Utility not found with id: " + id);
        }
    }

    // delete all utility
    public String deleteAllUtility() {
        utilityRepository.deleteAll();
        return "All utility has been deleted!";
    }

}
