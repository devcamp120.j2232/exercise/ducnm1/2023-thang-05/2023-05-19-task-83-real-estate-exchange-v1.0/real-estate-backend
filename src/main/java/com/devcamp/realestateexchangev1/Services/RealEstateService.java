package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.RealEstate;
import com.devcamp.realestateexchangev1.Repositories.IRealEstateRepository;

import javassist.NotFoundException;

@Service
public class RealEstateService {
    @Autowired
    private IRealEstateRepository realEstateRepository;

    // get all real estate
    public List<RealEstate> getAllRealEstate() {
        return realEstateRepository.findAll();
    }

    // get real estate by id
    public RealEstate getRealEstateById(int id) {
        return realEstateRepository.findById(id).get();
    }

    // create real estate
    public RealEstate createRealEstate(RealEstate realEstate) {
        return realEstateRepository.save(realEstate);
    }

    // update real estate 
    /*
     * public class RealEstate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private RealEstateType type;

    @Column(name = "title", length = 2000)
    private String title;

    @ManyToOne
    @JoinColumn(name = "request_id")
    private Request request;

    @ManyToOne
    @JoinColumn(name = "province_id")
    private Province province;

    @ManyToOne
    @JoinColumn(name = "district_id")
    private District district;

    @ManyToOne
    @JoinColumn(name = "ward_id")
    private Ward ward;

    @ManyToOne
    @JoinColumn(name = "street_id")
    private Street street;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Column(name = "address", length = 2000)
    private String address;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @Column(name = "price")
    private int price;

    @Column(name = "price_min")
    private int priceMin;

    @ManyToOne
    @JoinColumn(name = "price_time_id")
    private PriceTime priceTime;

    @Column(name = "date_create")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateCreate;

    @Column(name = "acreage")
    private double acreage;

    @ManyToOne
    @JoinColumn(name = "direction_id")
    private Direction direction;

    @Column(name = "total_floor")
    private int totalFloor;

    @Column(name = "number_floor")
    private int numberFloor;

    @Column(name = "bath")
    private int bath;

    @Column(name = "apart_code")
    private String apartCode;

    @Column(name = "wall_area")
    private double wallArea;

    @Column(name = "bedroom")
    private int bedroom;

    @Column(name = "balcony")
    private int balcony;

    @ManyToOne
    @JoinColumn(name = "landscape_view_id")
    private LandScapeView landscapeView;

    @ManyToOne
    @JoinColumn(name = "apart_loca_id")
    private ApartLoca apartLoca;

    @ManyToOne
    @JoinColumn(name = "apart_type_id")
    private ApartType apartType;

    @ManyToOne
    @JoinColumn(name = "furniture_type_id")
    private FurnitureType furnitureType;

    @Column(name = "price_rent")
    private int priceRent;

    @Column(name = "return_rate")
    private double returnRate;

    private int legalDoc;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "width_y")
    private int widthY;

    @Column(name = "long_x")
    private int longX;

    @Column(name = "street_house")
    private boolean streetHouse;

    @Column(name = "FSBO")
    private boolean FSBO;

    @Column(name = "view_num")
    private int viewNum;

    @Column(name = "create_by")
    private int createBy;

    @Column(name = "update_by")
    private int updateBy;

    @Column(name = "shape_id")
    private Shape shape;

    @Column(name = "distance2facade")
    private int distance2facade;

    @Column(name = "adjacent_facade_num")
    private int adjacentFacadeNum;

    @Column(name = "adjacent_road")
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private int alleyMinWidth;

    @Column(name = "adjacent_alley_min_width")
    private int adjacentAlleyMinWidth;

    @ManyToOne
    @JoinColumn(name = "factor_id")
    private Factor factor;

    @Column(name = "structure")
    private String structure;

    @Column(name = "DTSXD")
    private int DTSXD;

    @Column(name = "CLCL")
    private int CLCL;

    @Column(name = "CTXD_price")
    private int CTXDPrice;

    @Column(name = "CTXD_value")
    private int CTXDValue;

    @Column(name = "photo")
    private String photo;

    @Column(name = "_lat")
    private double lat;

    @Column(name = "_lng")
    private double lng;
}
     */
    public RealEstate updateRealEstate(int id, RealEstate realEstate) throws NotFoundException {
        Optional<RealEstate> optionalRealEstate = realEstateRepository.findById(id);
        if (!optionalRealEstate.isPresent()) {
            throw new NotFoundException("RealEstate not found with id " + id);
        }

        RealEstate foundRealEstate = optionalRealEstate.get();
        foundRealEstate.setType(realEstate.getType());
        foundRealEstate.setTitle(realEstate.getTitle());
        foundRealEstate.setRequest(realEstate.getRequest());
        foundRealEstate.setProvince(realEstate.getProvince());
        foundRealEstate.setDistrict(realEstate.getDistrict());
        foundRealEstate.setWard(realEstate.getWard());
        foundRealEstate.setStreet(realEstate.getStreet());
        foundRealEstate.setProject(realEstate.getProject());
        foundRealEstate.setAddress(realEstate.getAddress());
        foundRealEstate.setCustomer(realEstate.getCustomer());
        foundRealEstate.setPrice(realEstate.getPrice());
        foundRealEstate.setPriceMin(realEstate.getPriceMin());
        foundRealEstate.setPriceTime(realEstate.getPriceTime());
        foundRealEstate.setDateCreate(realEstate.getDateCreate());
        foundRealEstate.setAcreage(realEstate.getAcreage());
        foundRealEstate.setDirection(realEstate.getDirection());
        foundRealEstate.setTotalFloor(realEstate.getTotalFloor());
        foundRealEstate.setNumberFloor(realEstate.getNumberFloor());
        foundRealEstate.setBath(realEstate.getBath());
        foundRealEstate.setApartCode(realEstate.getApartCode());
        foundRealEstate.setWallArea(realEstate.getWallArea());
        foundRealEstate.setBedroom(realEstate.getBedroom());
        foundRealEstate.setBalcony(realEstate.getBalcony());
        foundRealEstate.setLandscapeView(realEstate.getLandscapeView());
        foundRealEstate.setApartLoca(realEstate.getApartLoca());
        foundRealEstate.setApartType(realEstate.getApartType());
        foundRealEstate.setFurnitureType(realEstate.getFurnitureType());
        foundRealEstate.setPriceRent(realEstate.getPriceRent());
        foundRealEstate.setReturnRate(realEstate.getReturnRate());
        foundRealEstate.setLegalDoc(realEstate.getLegalDoc());
        foundRealEstate.setDescription(realEstate.getDescription());
        foundRealEstate.setWidthY(realEstate.getWidthY());
        foundRealEstate.setLongX(realEstate.getLongX());
        foundRealEstate.setStreetHouse(realEstate.isStreetHouse());
        foundRealEstate.setFSBO(realEstate.isFSBO());
        foundRealEstate.setViewNum(realEstate.getViewNum());
        foundRealEstate.setCreateBy(realEstate.getCreateBy());
        foundRealEstate.setUpdateBy(realEstate.getUpdateBy());
        foundRealEstate.setShape(realEstate.getShape());
        foundRealEstate.setDistance2facade(realEstate.getDistance2facade());
        foundRealEstate.setAdjacentFacadeNum(realEstate.getAdjacentFacadeNum());
        foundRealEstate.setAdjacentRoad(realEstate.getAdjacentRoad());
        



        

        return realEstateRepository.save(foundRealEstate);
    }

    // delete real estate return result as a string
    public String deleteRealEstate(int id) throws NotFoundException {
        Optional<RealEstate> optionalRealEstate = realEstateRepository.findById(id);
        if (!optionalRealEstate.isPresent()) {
            throw new NotFoundException("RealEstate not found with id " + id);
        }

        realEstateRepository.deleteById(id);
        return "RealEstate with id " + id + " deleted successfully";
    }

    // delete all real estate return result as a string
    public String deleteAllRealEstate() {
        realEstateRepository.deleteAll();
        return "All RealEstate deleted successfully";
    }

}
