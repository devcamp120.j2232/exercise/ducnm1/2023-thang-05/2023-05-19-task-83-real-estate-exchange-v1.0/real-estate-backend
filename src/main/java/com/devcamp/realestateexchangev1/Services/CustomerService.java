package com.devcamp.realestateexchangev1.Services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Customer;
import com.devcamp.realestateexchangev1.Repositories.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private ICustomerRepository customerRepository;

    // get all customer
    public List<Customer> getAllCustomer() {
        return customerRepository.findAll();
    }

    // get customer by id
    public Customer getCustomerById(int id) {
        return customerRepository.findById(id).get();
    }

    private Customer saveCustomer(Customer customer) throws Exception {
        try {
            return customerRepository.save(customer);
        } catch (DataIntegrityViolationException ex) {
            // check email
            if (ex.getMessage().contains("customers.email")) {
                throw new DataIntegrityViolationException("Email đã tồn tại");
            }
            // check mobile
            if (ex.getMessage().contains("customers.mobile")) {
                throw new DataIntegrityViolationException("Số điện thoại đã tồn tại");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        return customer;
    }

    // create customer
    public Customer createCustomer(Customer customer) throws Exception {
        Customer newCustomer = new Customer();

        newCustomer.setContactName(customer.getContactName());
        newCustomer.setContactTitle(customer.getContactTitle());
        newCustomer.setAddress(customer.getAddress());
        newCustomer.setMobile(customer.getMobile());
        newCustomer.setEmail(customer.getEmail());
        newCustomer.setNote(customer.getNote());
        newCustomer.setCreateBy(customer.getCreateBy());
        // newCustomer.setUpdateBy(customer.getUpdateBy());
        newCustomer.setCreateDate(new Date());
        // newCustomer.setUpdateDate(customer.getUpdateDate());

        return saveCustomer(newCustomer);
    }

    // update customer
    public Customer updateCustomer(int id, Customer customer) throws Exception {
        Customer customerUpdate = customerRepository.findById(id).get();

        customerUpdate.setContactName(customer.getContactName());
        customerUpdate.setContactTitle(customer.getContactTitle());
        customerUpdate.setAddress(customer.getAddress());
        customerUpdate.setMobile(customer.getMobile());
        customerUpdate.setEmail(customer.getEmail());
        customerUpdate.setNote(customer.getNote());
        // customerUpdate.setCreateBy(customer.getCreateBy());
        customerUpdate.setUpdateBy(customer.getUpdateBy());
        // customerUpdate.setCreateDate(customer.getCreateDate());
        customerUpdate.setUpdateDate(new Date());

        return saveCustomer(customerUpdate);
    }

    // delete customer
    public String deleteCustomer(int id) {
        customerRepository.deleteById(id);
        return "Customer with id " + id + " deleted successfully";
    }

    // delete all customer
    public String deleteAllCustomer() {
        customerRepository.deleteAll();
        return "All Customer deleted successfully";
    }
}
