package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Subscription;
import com.devcamp.realestateexchangev1.Repositories.ISubscriptionRepository;

import javassist.NotFoundException;

@Service
public class SubscriptionService {
    @Autowired
    private ISubscriptionRepository subscriptionRepository;

    // get all subscription
    public List<Subscription> getAllSubscription() {
        return subscriptionRepository.findAll();
    }

    // get subscription by id
    public Subscription getSubscriptionById(int id) {
        return subscriptionRepository.findById(id).get();
    }

    // create subscription
    public Subscription createSubscription(Subscription subscription) {
        return subscriptionRepository.save(subscription);
    }

    // update subscription
    /*
     * public class Subcription {
     * 
     * @Id
     * 
     * @GeneratedValue(strategy = GenerationType.IDENTITY)
     * private int id;
     * 
     * @Column(name = "user")
     * private String user;
     * 
     * @Column(name = "endpoint")
     * private String endpoint;
     * 
     * @Column(name = "publickey")
     * private String publickey;
     * 
     * @Column(name = "authenticationtoken")
     * private String authenticationtoken;
     * 
     * @Column(name = "contentencoding")
     * private String contentencoding;
     * 
     * }
     */
    public Subscription updateSubscription(int id, Subscription subscription) throws NotFoundException {
        Optional<Subscription> optionalSubscription = subscriptionRepository.findById(id);
        if (!optionalSubscription.isPresent()) {
            throw new NotFoundException("Subscription not found with id " + id);
        }

        Subscription foundSubscription = optionalSubscription.get();
        foundSubscription.setUser(subscription.getUser());
        foundSubscription.setEndpoint(subscription.getEndpoint());
        foundSubscription.setPublickey(subscription.getPublickey());
        foundSubscription.setAuthenticationtoken(subscription.getAuthenticationtoken());
        foundSubscription.setContentencoding(subscription.getContentencoding());
        return subscriptionRepository.save(foundSubscription);
    }

    // delete subscription return a string
    public String deleteSubscription(int id) throws NotFoundException {
        Optional<Subscription> optionalSubscription = subscriptionRepository.findById(id);
        if (!optionalSubscription.isPresent()) {
            throw new NotFoundException("Subscription not found with id " + id);
        }
        subscriptionRepository.deleteById(id);
        return "Subscription deleted successfully";
    }

    // delete all subscription return a string
    public String deleteAllSubscription() {
        subscriptionRepository.deleteAll();
        return "All subscription deleted successfully";
    }

}
