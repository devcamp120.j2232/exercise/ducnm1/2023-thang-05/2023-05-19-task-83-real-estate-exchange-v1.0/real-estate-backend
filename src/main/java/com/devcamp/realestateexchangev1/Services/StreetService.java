package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Street;
import com.devcamp.realestateexchangev1.Repositories.IStreetRepository;

import javassist.NotFoundException;

@Service
public class StreetService {
    @Autowired
    private IStreetRepository streetRepository;

    // get all street
    public List<Street> getAllStreet() {
        return streetRepository.findAll();
    }

    // get street by id
    public Street getStreetById(int id) {
        return streetRepository.findById(id).get();
    }

    // get street by province id and district id
    public List<Street> getStreetByProvinceIdAndDistrictId(int provinceId, int districtId) {
        return streetRepository.findByProvinceIdAndDistrictId(provinceId, districtId);
    }

    // create street
    public Street createStreet(Street street) {
        return streetRepository.save(street);
    }

    // update street
    /*
     * public class Street {
     * 
     * @Id
     * 
     * @GeneratedValue(strategy = GenerationType.IDENTITY)
     * private int id;
     * 
     * @Column(name = "_name")
     * private String name;
     * 
     * @Column(name = "_prefix")
     * private String prefix;
     * 
     * // this is mapped by province class
     * 
     * @ManyToOne
     * 
     * @JoinColumn(name = "_province_id")
     * private Province province;
     * 
     * // this is mapped by district class
     * 
     * @ManyToOne
     * 
     * @JoinColumn(name = "_district_id")
     * private District district;
     * }
     */
    public Street updateStreet(int id, Street street) throws NotFoundException {
        Optional<Street> optionalStreet = streetRepository.findById(id);
        if (!optionalStreet.isPresent()) {
            throw new NotFoundException("Street not found with id " + id);
        }

        Street foundStreet = optionalStreet.get();
        foundStreet.setName(street.getName());
        foundStreet.setPrefix(street.getPrefix());
        foundStreet.setProvince(street.getProvince());
        foundStreet.setDistrict(street.getDistrict());
        return streetRepository.save(foundStreet);
    }

    // delete street return string
    public String deleteStreet(int id) throws NotFoundException {
        Optional<Street> optionalStreet = streetRepository.findById(id);
        if (!optionalStreet.isPresent()) {
            throw new NotFoundException("Street not found with id " + id);
        }

        streetRepository.deleteById(id);
        return "Street with " + id + " deleted successfully";
    }

    // delete all street return string
    public String deleteAllStreet() {
        streetRepository.deleteAll();
        return "All street deleted successfully";
    }

}
