package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.AddressMap;
import com.devcamp.realestateexchangev1.Repositories.IAddressMapRepository;

import javassist.NotFoundException;

@Service
public class AddressMapService {
    @Autowired
    private IAddressMapRepository addressMapRepository;

    public List<AddressMap> getAllAddressMap() {
        return addressMapRepository.findAll();
    }

    public AddressMap getAddressMapById(int id) throws NotFoundException {
        Optional<AddressMap> addressMapFound = addressMapRepository.findById(id);
        if (addressMapFound.isPresent()) {
            return addressMapFound.get();
        } else {
            throw new NotFoundException("AddressMap not found with id " + id);
        }
    }

    public AddressMap createAddressMap(AddressMap addressMap) {
        return addressMapRepository.save(addressMap);
    }

    public AddressMap updateAddressMap(int id, AddressMap addressMap) throws NotFoundException {
        Optional<AddressMap> addressMapFound = addressMapRepository.findById(id);
        if (addressMapFound.isPresent()) {
            AddressMap addressMapUpdate = addressMapFound.get();
            addressMapUpdate.setAddress(addressMap.getAddress());
            addressMapUpdate.setLat(addressMap.getLat());
            addressMapUpdate.setLng(addressMap.getLng());
            return addressMapRepository.save(addressMapUpdate);
        } else {
            throw new NotFoundException("AddressMap not found with id " + id);
        }
    }

    // delete method
    public String deleteAddressMap(int id) throws NotFoundException {
        Optional<AddressMap> addressMapFound = addressMapRepository.findById(id);
        if (addressMapFound.isPresent()) {
            addressMapRepository.deleteById(id);
            return "AddressMap with id " + id + " deleted successfully";
        } else {
            throw new NotFoundException("AddressMap not found with id " + id);
        }
    }

    // delete all method
    public String deleteAllAddressMap() {
        addressMapRepository.deleteAll();
        return "All AddressMap deleted successfully";
    }

}
