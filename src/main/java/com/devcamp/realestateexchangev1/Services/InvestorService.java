package com.devcamp.realestateexchangev1.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Investor;
import com.devcamp.realestateexchangev1.Repositories.IInvestorRepository;

@Service
public class InvestorService {
    @Autowired
    private IInvestorRepository investorRepository;

    // get all investor
    public List<Investor> getAllInvestor() {
        return investorRepository.findAll();
    }

    // get investor by id
    public Investor getInvestorById(int id) {
        return investorRepository.findById(id).get();
    }

    // create investor
    public Investor createInvestor(Investor investor) {
        return investorRepository.save(investor);
    }

    // update investor
    public Investor updateInvestor(int id, Investor investor) {
        Investor investorUpdate = investorRepository.findById(id).get();

        investorUpdate.setName(investor.getName());
        investorUpdate.setDescription(investor.getDescription());
        investorUpdate.setProjects(investor.getProjects());
        investorUpdate.setAddress(investor.getAddress());
        investorUpdate.setPhone(investor.getPhone());
        investorUpdate.setPhone2(investor.getPhone2());
        investorUpdate.setFax(investor.getFax());
        investorUpdate.setEmail(investor.getEmail());
        investorUpdate.setWebsite(investor.getWebsite());
        investorUpdate.setNote(investor.getNote());

        return investorRepository.save(investorUpdate);
    }

    // delete investor by id
    public String deleteInvestor(int id) {
        investorRepository.deleteById(id);
        return "Investor with id: " + id + " has been deleted!";
    }

    // delete all investor
    public String deleteAllInvestor() {
        investorRepository.deleteAll();
        return "All Investor has been deleted!";
    }

}
