package com.devcamp.realestateexchangev1.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.DesignUnit;
import com.devcamp.realestateexchangev1.Repositories.IDesignUnitRepository;

@Service
public class DesignUnitService {
    @Autowired
    private IDesignUnitRepository designUnitRepository;

    // get all designUnit
    public List<DesignUnit> getAllDesignUnit() {
        return designUnitRepository.findAll();
    }

    // get designUnit by id
    public DesignUnit getDesignUnitById(int id) {
        return designUnitRepository.findById(id).get();
    }

    // create designUnit
    public DesignUnit createDesignUnit(DesignUnit designUnit) {
        return designUnitRepository.save(designUnit);
    }

    // update designUnit
    public DesignUnit updateDesignUnit(int id, DesignUnit designUnit) {
        DesignUnit designUnitUpdate = designUnitRepository.findById(id).get();

        designUnitUpdate.setName(designUnit.getName());
        designUnitUpdate.setDescription(designUnit.getDescription());
        designUnitUpdate.setProjects(designUnit.getProjects());
        designUnitUpdate.setAddress(designUnit.getAddress());
        designUnitUpdate.setPhone(designUnit.getPhone());
        designUnitUpdate.setPhone2(designUnit.getPhone2());
        designUnitUpdate.setFax(designUnit.getFax());
        designUnitUpdate.setEmail(designUnit.getEmail());
        designUnitUpdate.setWebsite(designUnit.getWebsite());
        designUnitUpdate.setNote(designUnit.getNote());

        return designUnitRepository.save(designUnitUpdate);
    }

    // delete designUnit
    public String deleteDesignUnit(int id) {
        designUnitRepository.deleteById(id);
        return "DesignUnit with id " + id + " deleted successfully";
    }

    //delete all designUnit
    public String deleteAllDesignUnit(){
        designUnitRepository.deleteAll();
        return "All DesignUnit deleted successfully";
    }
}
