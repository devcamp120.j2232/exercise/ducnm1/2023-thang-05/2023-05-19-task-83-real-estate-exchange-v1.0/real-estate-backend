package com.devcamp.realestateexchangev1.Services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.MasterLayout;
import com.devcamp.realestateexchangev1.Repositories.IMasterLayoutRepository;

@Service
public class MasterLayoutService {
    @Autowired
    private IMasterLayoutRepository masterLayoutRepository;

    @Autowired
    private ProjectService projectService;

    // get all master layout
    public List<MasterLayout> getAllMasterLayout() {
        return masterLayoutRepository.findAll();
    }

    // get master layout by id
    public MasterLayout getMasterLayoutById(int id) {
        return masterLayoutRepository.findById(id).get();
    }

    // create master layout
    public MasterLayout createMasterLayout(int projectId, MasterLayout masterLayout) {
        MasterLayout newMasterLayout = new MasterLayout();
        newMasterLayout.setName(masterLayout.getName());
        newMasterLayout.setDescription(masterLayout.getDescription());
        newMasterLayout.setProject(projectService.getProjectById(projectId));
        newMasterLayout.setAcreage(masterLayout.getAcreage());
        newMasterLayout.setApartment_list(masterLayout.getApartment_list());
        newMasterLayout.setPhoto(masterLayout.getPhoto());
        newMasterLayout.setDate_create(new Date());
        // newMasterLayout.setDate_update(masterLayout.getDate_update());

        return masterLayoutRepository.save(newMasterLayout);
    }

    // update master layout
    public MasterLayout updateMasterLayout(int id, int projectId, MasterLayout masterLayout) {
        MasterLayout masterLayoutUpdate = masterLayoutRepository.findById(id).get();

        masterLayoutUpdate.setName(masterLayout.getName());
        masterLayoutUpdate.setDescription(masterLayout.getDescription());
        masterLayoutUpdate.setProject(projectService.getProjectById(projectId));
        masterLayoutUpdate.setAcreage(masterLayout.getAcreage());
        masterLayoutUpdate.setApartment_list(masterLayout.getApartment_list());
        masterLayoutUpdate.setPhoto(masterLayout.getPhoto());
        // masterLayoutUpdate.setDate_create(masterLayout.getDate_create());
        masterLayoutUpdate.setDate_update(new Date());

        return masterLayoutRepository.save(masterLayoutUpdate);
    }

    // delete master layout
    public String deleteMasterLayout(int id) {
        masterLayoutRepository.deleteById(id);
        return "Master Layout " + id + " was deleted";
    }

    // delete all master layout
    public String deleteAllMasterLayouts() {
        masterLayoutRepository.deleteAll();
        return "All Master Layouts were deleted";
    }

}
