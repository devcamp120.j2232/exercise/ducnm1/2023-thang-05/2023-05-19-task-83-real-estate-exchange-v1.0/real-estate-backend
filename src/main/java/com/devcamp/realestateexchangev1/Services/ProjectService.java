package com.devcamp.realestateexchangev1.Services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.realestateexchangev1.Entity.Project;
import com.devcamp.realestateexchangev1.Repositories.IConstructionContractorRepository;
import com.devcamp.realestateexchangev1.Repositories.IDesignUnitRepository;
import com.devcamp.realestateexchangev1.Repositories.IDistrictRepository;
import com.devcamp.realestateexchangev1.Repositories.IInvestorRepository;
import com.devcamp.realestateexchangev1.Repositories.IProjectRepository;
import com.devcamp.realestateexchangev1.Repositories.IProvinceRepository;
import com.devcamp.realestateexchangev1.Repositories.IStreetRepository;
import com.devcamp.realestateexchangev1.Repositories.IWardRepository;

import javassist.NotFoundException;

@Service
public class ProjectService {
    @Autowired
    private IProjectRepository projectRepository;
    @Autowired
    private IProvinceRepository provinceRepository;
    @Autowired
    private IDistrictRepository districtRepository;
    @Autowired
    private IWardRepository wardRepository;
    @Autowired
    private IStreetRepository streetRepository;
    @Autowired
    private IInvestorRepository investorRepository;
    @Autowired
    private IConstructionContractorRepository constructionContractorRepository;
    @Autowired
    private IDesignUnitRepository designUnitRepository;

    // get all project
    public List<Project> getAllProject() {
        return projectRepository.findAll();
    }

    // get project by id
    public Project getProjectById(int id) {
        return projectRepository.findById(id).get();
    }

    // create project
    public Project createProject(Project project) {
        Project newProject = new Project();
        newProject.setName(project.getName());
        newProject.setProvince(provinceRepository.findById(project.getProvince().getId()).get());
        newProject.setDistrict(districtRepository.findById(project.getDistrict().getId()).get());
        newProject.setWard(wardRepository.findById(project.getWard().getId()).get());
        newProject.setStreet(streetRepository.findById(project.getStreet().getId()).get());
        newProject.setAddress(project.getAddress());
        newProject.setSlogan(project.getSlogan());
        newProject.setDescription(project.getDescription());
        newProject.setAcreage(project.getAcreage());
        newProject.setConstructArea(project.getConstructArea());
        newProject.setNumBlock(project.getNumBlock());
        newProject.setNumFloors(project.getNumFloors());
        newProject.setNumApartment(project.getNumApartment());
        newProject.setApartmenttArea(project.getApartmenttArea());
        newProject.setInvestor(investorRepository.findById(project.getInvestor().getId()).get());
        newProject.setConstructionContractor(
                constructionContractorRepository.findById(project.getConstructionContractor().getId()).get());
        newProject.setDesignUnit(designUnitRepository.findById(project.getDesignUnit().getId()).get());
        newProject.setUtilities(project.getUtilities());
        newProject.setRegionLink(project.getRegionLink());
        newProject.setPhoto(project.getPhoto());
        newProject.setLat(project.getLat());
        newProject.setLng(project.getLng());

        return projectRepository.save(project);
    }

    // update project
    public Project updateProject(int id, Project project) throws NotFoundException {
        Optional<Project> optionalProject = projectRepository.findById(id);
        if (!optionalProject.isPresent()) {
            throw new NotFoundException("Project not found with id " + id);
        }

        Project foundProject = optionalProject.get();
        foundProject.setName(project.getName());
        foundProject.setProvince(project.getProvince());
        foundProject.setDistrict(project.getDistrict());
        foundProject.setWard(project.getWard());
        foundProject.setStreet(project.getStreet());
        foundProject.setAddress(project.getAddress());
        foundProject.setSlogan(project.getSlogan());
        foundProject.setDescription(project.getDescription());
        foundProject.setAcreage(project.getAcreage());
        foundProject.setConstructArea(project.getConstructArea());
        foundProject.setNumBlock(project.getNumBlock());
        foundProject.setNumFloors(project.getNumFloors());
        foundProject.setNumApartment(project.getNumApartment());
        foundProject.setApartmenttArea(project.getApartmenttArea());
        foundProject.setInvestor(project.getInvestor());
        foundProject.setConstructionContractor(project.getConstructionContractor());
        foundProject.setDesignUnit(project.getDesignUnit());
        foundProject.setUtilities(project.getUtilities());
        foundProject.setRegionLink(project.getRegionLink());
        foundProject.setPhoto(project.getPhoto());
        foundProject.setLat(project.getLat());
        foundProject.setLng(project.getLng());
        return projectRepository.save(foundProject);
    }

    // delete project
    public String deleteProject(int id) {
        projectRepository.deleteById(id);
        return "Project with id " + id + " deleted successfully";
    }

    // delete all project
    public String deleteAllProject() {
        projectRepository.deleteAll();
        return "All project deleted successfully";
    }

}
