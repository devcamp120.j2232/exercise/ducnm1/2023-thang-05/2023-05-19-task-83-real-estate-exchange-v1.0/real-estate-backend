package com.devcamp.realestateexchangev1.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.District;

public interface IDistrictRepository  extends JpaRepository<District, Integer>{
    List<District> findByProvinceId(int provinceId);
}
