package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.FurnitureType;

public interface IFurnitureTypeRepository extends JpaRepository<FurnitureType, Integer> {

}
