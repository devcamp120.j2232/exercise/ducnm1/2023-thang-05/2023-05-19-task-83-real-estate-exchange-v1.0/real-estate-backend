package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.ApartType;

public interface IApartTypeRepository extends JpaRepository<ApartType, Integer> {

}
