package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.RegionLink;

public interface IRegionLinkRepository  extends JpaRepository<RegionLink, Integer>{
    
}
