package com.devcamp.realestateexchangev1.Repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.Street;

public interface IStreetRepository extends JpaRepository<Street, Integer> {
    List<Street> findByProvinceIdAndDistrictId(int provinceId, int districtId);
}
