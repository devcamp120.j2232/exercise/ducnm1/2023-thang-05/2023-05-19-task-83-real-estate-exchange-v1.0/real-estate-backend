package com.devcamp.realestateexchangev1.Repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.Ward;

public interface IWardRepository extends JpaRepository<Ward, Integer> {
    public List<Ward> findByDistrictId(int districtId);
}
