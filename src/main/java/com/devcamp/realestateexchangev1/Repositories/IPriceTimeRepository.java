package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.PriceTime;

public interface IPriceTimeRepository extends JpaRepository<PriceTime, Integer> {

}
