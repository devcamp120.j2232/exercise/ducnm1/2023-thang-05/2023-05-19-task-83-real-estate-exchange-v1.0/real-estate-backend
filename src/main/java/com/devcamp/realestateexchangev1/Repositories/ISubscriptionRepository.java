package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.Subscription;

public interface ISubscriptionRepository extends JpaRepository<Subscription, Integer> {
    
}
