package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.RealEstateType;

public interface IRealEstateTypeRepository extends JpaRepository<RealEstateType, Integer> {

}
