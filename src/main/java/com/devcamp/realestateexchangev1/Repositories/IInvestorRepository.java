package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.Investor;

public interface IInvestorRepository  extends JpaRepository<Investor, Integer>{
    
}
