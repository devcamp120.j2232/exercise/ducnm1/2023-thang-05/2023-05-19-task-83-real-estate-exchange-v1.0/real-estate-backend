package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.Employee;

public interface IEmployeeRepository  extends JpaRepository<Employee, Integer>{
    
}
