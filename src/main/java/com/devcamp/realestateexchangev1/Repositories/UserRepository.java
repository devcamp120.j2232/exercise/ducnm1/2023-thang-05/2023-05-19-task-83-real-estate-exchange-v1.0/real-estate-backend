package com.devcamp.realestateexchangev1.Repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.User;



public interface UserRepository extends JpaRepository<User, Long> {
  Optional<User> findByUsername(String username);

  Boolean existsByUsername(String username);

  Boolean existsByEmail(String email);
}
