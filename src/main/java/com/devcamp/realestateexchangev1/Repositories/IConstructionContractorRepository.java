package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.ConstructionContractor;

public interface IConstructionContractorRepository extends JpaRepository<ConstructionContractor, Integer> {
    
}
