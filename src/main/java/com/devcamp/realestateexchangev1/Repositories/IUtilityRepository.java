package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.Utility;

public interface IUtilityRepository  extends JpaRepository<Utility, Integer>{
    
}
