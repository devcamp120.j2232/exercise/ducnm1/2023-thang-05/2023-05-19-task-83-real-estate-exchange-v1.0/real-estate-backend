package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.LandScapeView;

public interface ILandScapeViewRepository extends JpaRepository<LandScapeView, Integer> {

}
