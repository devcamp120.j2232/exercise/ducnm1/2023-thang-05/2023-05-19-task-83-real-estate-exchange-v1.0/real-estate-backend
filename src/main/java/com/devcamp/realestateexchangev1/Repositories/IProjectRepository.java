package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.Project;

public interface IProjectRepository  extends JpaRepository<Project, Integer>{
    
}
