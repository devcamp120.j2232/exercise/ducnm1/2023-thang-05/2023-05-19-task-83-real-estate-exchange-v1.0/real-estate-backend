package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.DesignUnit;

public interface IDesignUnitRepository  extends JpaRepository<DesignUnit, Integer>{
    
}
