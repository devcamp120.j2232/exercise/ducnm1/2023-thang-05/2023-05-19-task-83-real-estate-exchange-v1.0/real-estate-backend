package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.ApartLoca;

public interface IApartLocaRepository extends JpaRepository<ApartLoca, Integer> {

}
