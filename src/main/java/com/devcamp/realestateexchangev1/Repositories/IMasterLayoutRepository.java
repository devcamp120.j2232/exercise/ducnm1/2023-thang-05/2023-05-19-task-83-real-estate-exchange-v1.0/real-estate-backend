package com.devcamp.realestateexchangev1.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.realestateexchangev1.Entity.MasterLayout;

public interface IMasterLayoutRepository extends JpaRepository<MasterLayout, Integer>{
    
}
