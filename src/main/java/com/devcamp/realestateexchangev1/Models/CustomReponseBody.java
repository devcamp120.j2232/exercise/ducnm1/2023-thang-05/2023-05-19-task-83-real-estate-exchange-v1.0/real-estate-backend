package com.devcamp.realestateexchangev1.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomReponseBody {
    private String message;
    private Object body;

    public CustomReponseBody(String message) {
        this.message = message;
    }
}
