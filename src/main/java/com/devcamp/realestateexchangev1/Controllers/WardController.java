package com.devcamp.realestateexchangev1.Controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Ward;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.WardService;

@RestController
@CrossOrigin
@RequestMapping("/ward")
public class WardController {
    @Autowired
    private WardService wardService;

    // GetMapping get all ward return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllWard() {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, wardService.getAllWard()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // GetMapping get ward by id return response entity
    @GetMapping("/{id}")
    public ResponseEntity<Object> getWardById(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, wardService.getWardById(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // GetMapping get ward by district id return response entity
    @GetMapping("/byDistrictId/{id}")
    public ResponseEntity<Object> getWardByDistrictId(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, wardService.getWardByDistrictId(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PostMapping create ward return response entity
    @PostMapping("/create")
    public ResponseEntity<Object> createWard(@Valid @RequestBody Ward ward) {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Tạo mới thành công", wardService.createWard(ward)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PutMapping update ward return response entity
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateWard(@PathVariable int id, @Valid @RequestBody Ward ward) {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", wardService.updateWard(id, ward)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete ward return response entity
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteWard(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", wardService.deleteWard(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete all ward return response entity
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllWard() {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", wardService.deleteAllWard()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }
}
