package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Employee;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.EmployeeService;

@RestController
@CrossOrigin
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    // getmapping method get all employee
    @GetMapping("/all")
    public ResponseEntity<Object> getAllEmployee() {
        try {
            List<Employee> employee = employeeService.getAllEmployee();
            return ResponseEntity.ok(new CustomReponseBody(null, employee));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // getmapping method get employee by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable int id) {
        try {
            Employee employee = employeeService.getEmployeeById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, employee));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Postmapping method create new employee
    @PostMapping("/create")
    public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee employee) {
        try {
            Employee employeeCreated = employeeService.createEmployee(employee);
            return ResponseEntity.ok(new CustomReponseBody("Tạo mới thành công", employeeCreated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Putmapping method update employee by id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable int id, @Valid @RequestBody Employee employee) {
        try {
            Employee employeeUpdated = employeeService.updateEmployee(id, employee);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", employeeUpdated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Deletemapping method delete employee by id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteEmployee(@PathVariable int id) {
        try {
            String result = employeeService.deleteEmployee(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Deletemapping method delete all employee
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllEmployee() {
        try {
            String result = employeeService.deleteAllEmployee();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }
}
