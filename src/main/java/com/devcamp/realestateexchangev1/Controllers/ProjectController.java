package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Project;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.ProjectService;

@RestController
@CrossOrigin
@RequestMapping("/project")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    // get all project return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllProject() {
        try {
            List<Project> projects = projectService.getAllProject();
            return ResponseEntity.ok(new CustomReponseBody(null, projects));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Get a project by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getProjectById(@PathVariable("id") int id) {
        try {
            Project project = projectService.getProjectById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, project));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Create a new project
    @PostMapping("/create")
    public ResponseEntity<Object> createProject(@Valid @RequestBody Project project) {
        try {
            Project newProject = projectService.createProject(project);
            return ResponseEntity.ok(new CustomReponseBody("Thêm mới thành công", newProject));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Update a project
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateProject(@PathVariable("id") int id, @Valid @RequestBody Project project) {
        try {
            Project updatedProject = projectService.updateProject(id, project);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", updatedProject));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Delete a project
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteProject(@PathVariable("id") int id) {
        try {
            String result = projectService.deleteProject(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Delete all projects
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllProject() {
        try {
            String result = projectService.deleteAllProject();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

}
