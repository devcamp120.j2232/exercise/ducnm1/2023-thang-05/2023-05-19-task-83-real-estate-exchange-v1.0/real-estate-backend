package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.DesignUnit;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.DesignUnitService;

@RestController
@CrossOrigin
@RequestMapping("/designunit")
public class DesignUnitController {
    @Autowired
    private DesignUnitService designunitservice;

    // get mapping method get all design unit
    @GetMapping("/all")
    public ResponseEntity<Object> getAllDesignUnit() {
        try {
            List<DesignUnit> designunit = designunitservice.getAllDesignUnit();
            return ResponseEntity.ok(new CustomReponseBody(null, designunit));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // get mapping method get design unit by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getDesignUnitById(@PathVariable int id) {
        try {
            DesignUnit designunit = designunitservice.getDesignUnitById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, designunit));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // create method
    @PostMapping("/create")
    public ResponseEntity<Object> createDesignUnit(@Valid @RequestBody DesignUnit designUnit) {
        try {
            DesignUnit designUnitCreated = designunitservice.createDesignUnit(designUnit);
            return ResponseEntity.ok(new CustomReponseBody("Thêm mới thành công", designUnitCreated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // update method
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateDesignUnit(@PathVariable int id, @Valid @RequestBody DesignUnit designUnit) {
        try {
            DesignUnit designUnitUpdated = designunitservice.updateDesignUnit(id, designUnit);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", designUnitUpdated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // delete method
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteDesignUnit(@PathVariable int id) {
        try {
            String deleteDesignUnit = designunitservice.deleteDesignUnit(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", deleteDesignUnit));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // delete all method
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllDesignUnit() {
        try {
            String deleteAllDesignUnit = designunitservice.deleteAllDesignUnit();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", deleteAllDesignUnit));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

}
