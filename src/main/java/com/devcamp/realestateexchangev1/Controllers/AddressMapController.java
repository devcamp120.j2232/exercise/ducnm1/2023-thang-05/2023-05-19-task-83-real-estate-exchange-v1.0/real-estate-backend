package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.AddressMap;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.AddressMapService;

@RestController
@CrossOrigin
@RequestMapping("/addressmap")
public class AddressMapController {
    @Autowired
    private AddressMapService addressMapService;

    // get mapping method get all address map
    @GetMapping("/all")
    public ResponseEntity<Object> getAllAddressMap() {
        try {
            List<AddressMap> addressMap = addressMapService.getAllAddressMap();
            return ResponseEntity.ok(addressMap);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // get mapping method get address map by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getAddressMapById(@PathVariable int id) {
        try {
            AddressMap addressMap = addressMapService.getAddressMapById(id);
            return ResponseEntity.ok(addressMap);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // create method
    @PostMapping("/create")
    public ResponseEntity<Object> createAddressMap(@Valid @RequestBody AddressMap addressMap) {
        try {
            AddressMap addressMapCreated = addressMapService.createAddressMap(addressMap);
            return ResponseEntity.ok(new CustomReponseBody("Thêm mới thành công", addressMapCreated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // update method
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateAddressMap(@PathVariable int id, @RequestBody AddressMap addressMap) {
        try {
            AddressMap addressMapUpdated = addressMapService.updateAddressMap(id, addressMap);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", addressMapUpdated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // delete method
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteAddressMap(@PathVariable int id) {
        try {
            String addressMapDeleted = addressMapService.deleteAddressMap(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", addressMapDeleted));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(new CustomReponseBody("Xóa không thành công", e.getCause().getCause().getMessage()));
        }
    }

    // delete all method
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllAddressMap() {
        try {
            String addressMapDeleted = addressMapService.deleteAllAddressMap();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", addressMapDeleted));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(new CustomReponseBody("Xóa tất cả không thành công", e.getCause().getCause().getMessage()));
        }
    }
}
