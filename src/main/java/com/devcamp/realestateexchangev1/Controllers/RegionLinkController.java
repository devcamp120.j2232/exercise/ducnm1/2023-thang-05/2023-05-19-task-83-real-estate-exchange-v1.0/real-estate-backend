package com.devcamp.realestateexchangev1.Controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.RegionLink;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.RegionLinkService;

@RestController
@CrossOrigin
@RequestMapping("/regionlink")
public class RegionLinkController {
    @Autowired
    private RegionLinkService regionLinkService;

    // GetMapping get all region link return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllRegionLink() {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, regionLinkService.getAllRegionLink()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // GetMapping get region link by id return response entity
    @GetMapping("/{id}")
    public ResponseEntity<Object> getRegionLinkById(@PathVariable int id) {
        try {
            return ResponseEntity.ok(regionLinkService.getRegionLinkById(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PostMapping create region link return response entity
    @PostMapping("/create")
    public ResponseEntity<Object> createRegionLink(@Valid @RequestBody RegionLink regionLink) {
        try {
            return ResponseEntity
                    .ok(new CustomReponseBody("Thêm mới thành công", regionLinkService.createRegionLink(regionLink)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PutMapping update region link return response entity
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateRegionLink(@PathVariable int id, @Valid @RequestBody RegionLink regionLink) {
        try {
            return ResponseEntity.ok(
                    new CustomReponseBody("Cập nhật thành công", regionLinkService.updateRegionLink(id, regionLink)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete region link return response entity
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteRegionLink(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", regionLinkService.deleteRegionLink(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete all region link return response entity
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllRegionLink() {
        try {
            return ResponseEntity
                    .ok(new CustomReponseBody("Xóa tất cả thành công", regionLinkService.deleteAllRegionLink()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

}
