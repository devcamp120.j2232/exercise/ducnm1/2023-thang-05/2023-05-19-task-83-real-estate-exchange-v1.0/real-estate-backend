package com.devcamp.realestateexchangev1.Controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Street;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.StreetService;

@RestController
@CrossOrigin
@RequestMapping("/street")
public class StreetController {
    @Autowired
    private StreetService streetService;

    // GetMapping get all street return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllStreet() {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, streetService.getAllStreet()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // GetMapping get street by id return response entity
    @GetMapping("/{id}")
    public ResponseEntity<Object> getStreetById(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, streetService.getStreetById(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // GetMapping get street by provinceId and districtId
    @GetMapping("/byProvinceIdAndDistrictId/{provinceId}/{districtId}")
    public ResponseEntity<Object> getStreetByProvinceIdAndDistrictId(@PathVariable int provinceId,
            @PathVariable int districtId) {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null,
                    streetService.getStreetByProvinceIdAndDistrictId(provinceId, districtId)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PostMapping create street return response entity
    @PostMapping("/create")
    public ResponseEntity<Object> createStreet(@Valid @RequestBody Street street) {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Tạo mới thành công", streetService.createStreet(street)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PutMapping update street return response entity
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateStreet(@PathVariable int id, @Valid @RequestBody Street street) {
        try {
            return ResponseEntity
                    .ok(new CustomReponseBody("Cập nhật thành công", streetService.updateStreet(id, street)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete street return response entity
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteStreet(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", streetService.deleteStreet(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete all street return response entity
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllStreet() {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", streetService.deleteAllStreet()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

}
