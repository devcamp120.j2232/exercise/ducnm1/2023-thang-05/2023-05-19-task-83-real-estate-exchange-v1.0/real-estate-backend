package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.MasterLayout;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.MasterLayoutService;

@RestController
@CrossOrigin
@RequestMapping("/masterlayout")
public class MasterLayoutController {
    @Autowired
    private MasterLayoutService masterLayoutService;

    // get all master layout return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllMasterLayout() {
        try {
            List<MasterLayout> masterLayouts = masterLayoutService.getAllMasterLayout();
            return ResponseEntity.ok(masterLayouts);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // get master layout by id return response entity
    @GetMapping("/{id}")
    public ResponseEntity<Object> getMasterLayoutById(@PathVariable int id) {
        try {
            MasterLayout masterLayout = masterLayoutService.getMasterLayoutById(id);
            return ResponseEntity.ok(masterLayout);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // create master layout return response entity
    @PostMapping("/create")
    public ResponseEntity<Object> createMasterLayout(@RequestParam("projectId") int projectId,
            @Valid @RequestBody MasterLayout masterLayout) {
        try {
            MasterLayout newMasterLayout = masterLayoutService.createMasterLayout(projectId, masterLayout);
            return ResponseEntity.ok(new CustomReponseBody("Thêm mới thành công", newMasterLayout));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // update master layout return response entity
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateMasterLayout(@PathVariable int id, @RequestParam("projectId") int projectId,
            @Valid @RequestBody MasterLayout masterLayout) {
        try {
            MasterLayout masterLayoutUpdate = masterLayoutService.updateMasterLayout(id, projectId, masterLayout);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", masterLayoutUpdate));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete master layout return response entity
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteMasterLayout(@PathVariable int id) {
        try {
            String deleteMasterLayout = masterLayoutService.deleteMasterLayout(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", deleteMasterLayout));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete all master layout return response entity
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllMasterLayouts() {
        try {
            String deleteAllMasterLayouts = masterLayoutService.deleteAllMasterLayouts();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

}
