package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Customer;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.CustomerService;

@RestController
@CrossOrigin
@RequestMapping("/customer")
public class CustomerController {
    // auto wired customer service
    @Autowired
    private CustomerService customerService;

    // get mapping method get all customer
    @GetMapping("/all")
    public ResponseEntity<Object> getAllCustomer() {
        try {
            List<Customer> customer = customerService.getAllCustomer();
            return ResponseEntity.ok(new CustomReponseBody(null, customer));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // get mapping method get customer by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable int id) {
        try {
            Customer customer = customerService.getCustomerById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, customer));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // create method
    @PostMapping("/create")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer customer) {
        try {
            Customer customerCreated = customerService.createCustomer(customer);
            return ResponseEntity.ok(new CustomReponseBody("Thêm mới thành công", customerCreated));
        } catch (DataIntegrityViolationException ex) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(ex.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // update method
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateCustomer(@PathVariable int id, @Valid @RequestBody Customer customer) {
        try {
            Customer customerUpdated = customerService.updateCustomer(id, customer);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", customerUpdated));
        } catch (DataIntegrityViolationException ex) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(ex.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete method
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteCustomer(@PathVariable int id) {
        try {
            String customerDeleted = customerService.deleteCustomer(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", customerDeleted));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete all method
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllCustomer() {
        try {
            String customerDeleted = customerService.deleteAllCustomer();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", customerDeleted));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

}
