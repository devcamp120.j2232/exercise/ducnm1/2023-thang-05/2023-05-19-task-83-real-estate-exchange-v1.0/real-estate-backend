package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.District;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/district")
public class DistrictController {
    @Autowired
    private DistrictService districtService;

    // get mapping method get all district
    @GetMapping("/all")
    public ResponseEntity<Object> getAllDistrict() {
        try {
            List<District> district = districtService.getAllDistrict();
            return ResponseEntity.ok(new CustomReponseBody(null, district));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // get mapping method get district by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getDistrictById(@PathVariable int id) {
        try {
            District district = districtService.getDistrictById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, district));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // get mapping method get district by province id
    @GetMapping("/byProvinceId/{id}")
    public ResponseEntity<Object> getDistrictByProvinceId(@PathVariable int id) {
        try {
            List<District> districts = districtService.getDistrictByProvinceId(id);
            return ResponseEntity.ok(new CustomReponseBody(null, districts));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // create new district
    @PostMapping("/create")
    public ResponseEntity<Object> createDistrict(@Valid @RequestBody District district) {
        try {
            District districtCreated = districtService.createDistrict(district);
            return ResponseEntity.ok(new CustomReponseBody("Tạo mới thành công", districtCreated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // update district by id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateDistrict(@PathVariable int id, @Valid @RequestBody District district) {
        try {
            District districtUpdated = districtService.updateDistrict(id, district);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", districtUpdated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete district by id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteDistrict(@PathVariable int id) {
        try {
            String result = districtService.deleteDistrict(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete all district
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllDistrict() {
        try {
            String result = districtService.deleteAllDistrict();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }
}
