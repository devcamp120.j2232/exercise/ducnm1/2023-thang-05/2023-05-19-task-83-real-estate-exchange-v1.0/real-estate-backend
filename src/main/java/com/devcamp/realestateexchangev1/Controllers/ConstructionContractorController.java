package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.ConstructionContractor;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.ConstructionContractorService;

@RestController
@CrossOrigin
@RequestMapping("/constructioncontractor")
public class ConstructionContractorController {
    @Autowired
    private ConstructionContractorService constructioncontractorservice;

    // get mapping method get all construction contractor
    @GetMapping("/all")
    public ResponseEntity<Object> getAllConstructionContractor() {
        try {
            List<ConstructionContractor> constructioncontractor = constructioncontractorservice
                    .getAllConstructionContractor();
            return ResponseEntity.ok(new CustomReponseBody(null, constructioncontractor));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // get mapping method get construction contractor by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getConstructionContractorById(@PathVariable int id) {
        try {
            ConstructionContractor constructioncontractor = constructioncontractorservice
                    .getConstructionContractorById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, constructioncontractor));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // create method
    @PostMapping("/create")
    public ResponseEntity<Object> createConstructionContractor(
            @Valid @RequestBody ConstructionContractor constructioncontractor) {
        try {
            ConstructionContractor constructioncontractorCreated = constructioncontractorservice
                    .createConstructionContractor(constructioncontractor);
            return ResponseEntity.ok(new CustomReponseBody("Tạo mới thành công", constructioncontractorCreated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // update method
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateConstructionContractor(@PathVariable int id,
            @RequestBody ConstructionContractor constructioncontractor) {
        try {
            ConstructionContractor constructioncontractorUpdated = constructioncontractorservice
                    .updateConstructionContractor(id, constructioncontractor);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", constructioncontractorUpdated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete method
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteConstructionContractor(@PathVariable int id) {
        try {
            String constructioncontractorDeleted = constructioncontractorservice.deleteConstructionContractor(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // delete all method
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllConstructionContractor() {
        try {
            String constructioncontractorDeleted = constructioncontractorservice.deleteAllConstructionContractor();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công"));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }
}