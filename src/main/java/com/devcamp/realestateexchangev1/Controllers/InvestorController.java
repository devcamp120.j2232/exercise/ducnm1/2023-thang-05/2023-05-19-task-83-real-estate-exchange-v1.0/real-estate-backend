package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Investor;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.InvestorService;

@RestController
@CrossOrigin
@RequestMapping("/investor")
public class InvestorController {
    @Autowired
    private InvestorService investorService;

    // getmapping method get all investor
    @GetMapping("/all")
    public ResponseEntity<Object> getAllInvestor() {
        try {
            List<Investor> investors = investorService.getAllInvestor();
            return ResponseEntity.ok(new CustomReponseBody(null, investors));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // getmapping method get investor by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getInvestorById(@PathVariable int id) {
        try {
            Investor investor = investorService.getInvestorById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, investor));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // postmapping method create new investor
    @PostMapping("/create")
    public ResponseEntity<Object> createInvestor(@Valid @RequestBody Investor investor) {
        try {
            Investor investorCreated = investorService.createInvestor(investor);
            return ResponseEntity.ok(new CustomReponseBody("Thêm mới thành công", investorCreated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // putmapping method update investor by id
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateInvestor(@PathVariable int id, @Valid @RequestBody Investor investor) {
        try {
            Investor investorUpdated = investorService.updateInvestor(id, investor);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", investorUpdated));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // deletemapping method delete investor by id
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteInvestor(@PathVariable int id) {
        try {
            String investorDeleted = investorService.deleteInvestor(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", investorDeleted));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // deletemapping method delete all investor
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllInvestor() {
        try {
            String investorDeleted = investorService.deleteAllInvestor();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", investorDeleted));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

}
