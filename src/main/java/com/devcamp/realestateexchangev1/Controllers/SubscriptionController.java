package com.devcamp.realestateexchangev1.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Subscription;
import com.devcamp.realestateexchangev1.Services.SubscriptionService;

@RestController
@CrossOrigin
@RequestMapping("/subscription")
public class SubscriptionController {
    @Autowired
    private SubscriptionService subscriptionService;

    // GetMapping get all subscription return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllSubscription() {
        try {
            return ResponseEntity.ok(subscriptionService.getAllSubscription());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // GetMapping get subscription by id return response entity
    @GetMapping("/{id}")
    public ResponseEntity<Object> getSubscriptionById(@PathVariable int id) {
        try {
            return ResponseEntity.ok(subscriptionService.getSubscriptionById(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // PostMapping create subscription return response entity
    @PostMapping("/create")
    public ResponseEntity<Object> createSubscription(@RequestBody Subscription subscription) {
        try {
            return ResponseEntity.ok(subscriptionService.createSubscription(subscription));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // PutMapping update subscription return response entity
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateSubscription(@PathVariable int id, @RequestBody Subscription subscription) {
        try {
            return ResponseEntity.ok(subscriptionService.updateSubscription(id, subscription));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // DeleteMapping delete subscription return response entity
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteSubscription(@PathVariable int id) {
        try {
            return ResponseEntity.ok(subscriptionService.deleteSubscription(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // DeleteMapping delete all subscription return response entity
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllSubscription() {
        try {
            return ResponseEntity.ok(subscriptionService.deleteAllSubscription());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }
}
