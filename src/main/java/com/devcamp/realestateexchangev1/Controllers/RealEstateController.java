package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.RealEstate;
import com.devcamp.realestateexchangev1.Services.RealEstateService;

@RestController
@CrossOrigin
@RequestMapping("/realestate")
public class RealEstateController {
    @Autowired
    private RealEstateService realEstateService;

    // get all real estate return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllRealEstate() {
        try {
            List<RealEstate> realEstates = realEstateService.getAllRealEstate();
            return ResponseEntity.ok(realEstates);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // Get a real estate by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getRealEstateById(@PathVariable("id") int id) {
        try {
            RealEstate realEstate = realEstateService.getRealEstateById(id);
            return ResponseEntity.ok(realEstate);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // Create a new real estate
    @PostMapping("/create")
    public ResponseEntity<Object> createRealEstate(@RequestBody RealEstate realEstate) {
        try {
            RealEstate newRealEstate = realEstateService.createRealEstate(realEstate);
            return ResponseEntity.ok(newRealEstate);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // Update a real estate
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateRealEstate(@PathVariable("id") int id, @RequestBody RealEstate realEstate) {
        try {
            RealEstate updateRealEstate = realEstateService.updateRealEstate(id, realEstate);
            return ResponseEntity.ok(updateRealEstate);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // Delete a real estate
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteRealEstate(@PathVariable("id") int id) {
        try {
            String result = realEstateService.deleteRealEstate(id);
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // Delete all real estate return response entity string
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllRealEstate() {
        try {
            String result = realEstateService.deleteAllRealEstate();
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

}
