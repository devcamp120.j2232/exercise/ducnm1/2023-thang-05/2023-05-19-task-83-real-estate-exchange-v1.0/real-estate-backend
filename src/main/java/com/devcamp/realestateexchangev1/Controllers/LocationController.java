package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Location;
import com.devcamp.realestateexchangev1.Services.LocationService;

@RestController
@CrossOrigin
@RequestMapping("/location")
public class LocationController {
    @Autowired
    private LocationService locationService;

    // getmapping method get all location
    @GetMapping("/all")
    public ResponseEntity<Object> getAllLocation() {
        try {
            List<Location> location = locationService.getAllLocation();
            return ResponseEntity.ok(location);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // getmapping method get location by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getLocationById(@PathVariable int id) {
        try {
            Location location = locationService.getLocationById(id);
            return ResponseEntity.ok(location);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // create location
    @PostMapping("/create")
    public ResponseEntity<Object> createLocation(@RequestBody Location location) {
        try {
            Location newLocation = locationService.createLocation(location);
            return ResponseEntity.ok(newLocation);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // update location
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateLocation(@PathVariable int id, @RequestBody Location location) {
        try {
            Location locationUpdated = locationService.updateLocation(id, location);
            return ResponseEntity.ok(locationUpdated);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // delete location
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteLocation(@PathVariable int id) {
        try {
            String result = locationService.deleteLocation(id);
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

    // delete all location
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllLocation() {
        try {
            String result = locationService.deleteAllLocation();
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getCause().getCause().getMessage());
        }
    }

}
