package com.devcamp.realestateexchangev1.Controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Province;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.ProvinceService;

@RestController
@CrossOrigin
@RequestMapping("/province")
public class ProvinceController {
    @Autowired
    private ProvinceService provinceService;

    // get all province return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllProvince() {
        try {
            List<Province> provinces = provinceService.getAllProvince();
            return ResponseEntity.ok(new CustomReponseBody(null, provinces));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Getmapping get a province by id
    @GetMapping("/{id}")
    public ResponseEntity<Object> getProvinceById(@PathVariable("id") int id) {
        try {
            Province province = provinceService.getProvinceById(id);
            return ResponseEntity.ok(new CustomReponseBody(null, province));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Postmapping create a new province
    @PostMapping("/create")
    public ResponseEntity<Object> createProvince(@Valid @RequestBody Province province) {
        try {
            Province newProvince = provinceService.createProvince(province);
            return ResponseEntity.ok(new CustomReponseBody("Tạo mới thành công", newProvince));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Putmapping update a province
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateProvince(@PathVariable("id") int id, @Valid @RequestBody Province province) {
        try {
            Province updatedProvince = provinceService.updateProvince(id, province);
            return ResponseEntity.ok(new CustomReponseBody("Cập nhật thành công", updatedProvince));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // Deletemapping delete a province
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteProvince(@PathVariable("id") int id) {
        try {
            String result = provinceService.deleteProvince(id);
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete all province
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllProvince() {
        try {
            String result = provinceService.deleteAllProvince();
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", result));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }
}
