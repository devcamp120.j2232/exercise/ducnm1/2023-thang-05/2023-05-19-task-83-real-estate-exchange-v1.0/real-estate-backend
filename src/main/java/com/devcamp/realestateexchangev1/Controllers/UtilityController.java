package com.devcamp.realestateexchangev1.Controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.realestateexchangev1.Entity.Utility;
import com.devcamp.realestateexchangev1.Models.CustomReponseBody;
import com.devcamp.realestateexchangev1.Services.UtilityService;

@RestController
@CrossOrigin
@RequestMapping("/utilities")
public class UtilityController {
    @Autowired
    private UtilityService utilityService;

    // GetMapping get all utility return response entity
    @GetMapping("/all")
    public ResponseEntity<Object> getAllUtility() {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, utilityService.getAllUtility()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // GetMapping get utility by id return response entity
    @GetMapping("/{id}")
    public ResponseEntity<Object> getUtilityById(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody(null, utilityService.getUtilityById(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PostMapping create utility return response entity
    @PostMapping("/create")
    public ResponseEntity<Object> createUtility(@Valid @RequestBody Utility utility) {
        try {
            return ResponseEntity
                    .ok(new CustomReponseBody("Tạo mới thành công", utilityService.createUtility(utility)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // PutMapping update utility return response entity
    @PutMapping("/update/{id}")
    public ResponseEntity<Object> updateUtility(@PathVariable int id, @Valid @RequestBody Utility utility) {
        try {
            return ResponseEntity
                    .ok(new CustomReponseBody("Cập nhật thành công", utilityService.updateUtility(id, utility)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping delete utility return response entity
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUtility(@PathVariable int id) {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Xóa thành công", utilityService.deleteUtility(id)));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }

    // DeleteMapping all utility return response entity
    @DeleteMapping("/deleteall")
    public ResponseEntity<Object> deleteAllUtility() {
        try {
            return ResponseEntity.ok(new CustomReponseBody("Xóa tất cả thành công", utilityService.deleteAllUtility()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new CustomReponseBody(e.getCause().getCause().getMessage()));
        }
    }
}
